$( document ).ready(function() {
	var baselink = $("#baselink").val();
	$("#firstRadio").prop("checked",true);

	displayList();
	setValuePicker();
	function displayList(){
		$.ajax({
			type: "POST",
			url: baselink+'detailPekerjaan/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item,count);
					count++;
				});
				$("#pekerjaanTable").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});  
	}	
	function parseList(data,count){
		$('#actionItemTemplate').find('#editList').attr("data-id",data.id_detail_pekerjaan);
		$('#actionItemTemplate').find('#deleteList').attr("data-id",data.id_detail_pekerjaan);
		$('#actionItemTemplate').find('#detailList').attr("data-id",data.id_detail_pekerjaan);
		var text = 	'<tr>'+
					'<td>'+count+'</td>'+
					'<td>'+data.nama_pekerjaan+'</td>'+
					'<td>'+data.nama_detail_pekerjaan+'</td>'+
					'<td>'+data.jenis_pekerjaan+'</td>'+				
					'<td style="display:flex;">'+
						$("#actionItemTemplate").html()+
					'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	});	
	function resetModal(){
		$('input[type="text"]').val('');
		$('textarea').val('');
		$("#inputPekerjaan").val($("#inputPekerjaan option:first").val());
		$("#firstRadio").prop("checked",true);
		$("#manipulateForm").prop('action',baselink+'detailPekerjaan/add');
		$("#modalTitle").html("Tambah Data Detail Pekerjaan");
	}
	function setValuePicker(){
			$.ajax({
				type: "POST",
				url: baselink+'listPekerjaan',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					$.each(data,function(i,item){
						$('#inputPekerjaan').append('<option value="'+item.id+'" >'+item.nama+'</option>')
					});						
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		if ($("#inputDeskripsi").val()==""){
			alert("Deskripsi harus diisi");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Detail pekerjaan berhasil disimpan");
						$("#manipulateModal").modal("hide");
						$("#pekerjaanTable").DataTable().destroy();
						displayList();
					}else{
						alert("Gagal menyimpan data pekerjaan");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});	
	$("#listView").on('click','#editList',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});
	$("#listView").on('click','#detailList',function(){
		var id = $(this).attr('data-id');
		getDetail(id,1);
	});		
	function getDetail(id,isDetail = 0){
		$.ajax({
			type: "POST",
			url: baselink+'detailPekerjaan/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				parseDetail(data[0],isDetail);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data,isDetail = 0){
			if(isDetail == 0){
				$("#inputName").val(data.nama_detail_pekerjaan);
				$("#inputDeskripsi").val(data.deskripsi);
				$("#inputPekerjaan option[value="+data.id_pekerjaan+"]").prop("selected",true);
				$("#editIdPekerjaan").val(data.id_detail_pekerjaan);
				if(data.jenis_pekerjaan == 0){
					$("#firstRadio").prop("checked",true);
				}else {
					$("#secondRadio").prop("checked",true);
				}
				$("#manipulateModal").modal("show");
				$("#manipulateForm").prop('action',baselink+'detailPekerjaan/edit');
				$("#modalTitle").html("Ubah Data Detail Pekerjaan");
			} else{
				$("#detailName").val(data.nama_pekerjaan);
				$("#detailDeskripsi").val(data.deskripsi);
				$("#detailDetailPekerjaan").val(data.nama_detail_pekerjaan);
				$("#detailCreateAt").val(data.created_at);
				$("#detailUpdateAt").val(data.updated_at);
				$("#detailUpdateBy").val(data.updated_by);
				$("#detailModal").modal("show");				
			}		
	}

	$("#listView").on('click','#deleteList',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'detailPekerjaan/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#pekerjaanTable").DataTable().destroy();
					displayList()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});			
});