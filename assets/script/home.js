$( document ).ready(function() {
	var baselink = $('#baselink').val();
	var dataClient = null;
	var dataLength = 6;
	var currentPage = 1;
	loadCheckPekerjaan();
	getListClient();
	setValuePicker();
	getCount();
	loadData();
	$('#startContract').datepicker({
	   autoclose: true
	})
	$('#endContract').datepicker({
	   autoclose: true
	})
	function getListClient(){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'listClient',
				cache: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					dataClient = data;
					clearContainer();
					currentPage = 1;
					pagging();							
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	function pagging(){
		var text = '<li class="paginate_button previous disabled" id="viewClient_previous">'+
                                  '<a href="#" aria-controls="tableLog" data-dt-idx="0" tabindex="0">Previous</a>'+
                                '</li>'+
                                '<li class="paginate_button next" id="viewClient_next">'+
                                  '<a href="#" aria-controls="tableLog" data-dt-idx="8" tabindex="0">Next</a>'+
                                '</li>';

		var a = 1;
		var x = dataClient.length;
		if (x < dataLength){
			a = 1;
		}else if ((x % dataLength) > 0) {
			a = a + Math.trunc(x/dataLength);
		} else {
			a = a * (x/dataLength);
		}
		$("#paginationContainer").html(text);
		var before = parseInt(currentPage) - 1;
		if (before<1){
			before = 1;
		}
		var after = parseInt(currentPage) + 1;
		if(after>a){
			after = a;
		}
		$("#viewClient_previous").find('a').attr('data-dt-idx',before);
		$("#viewClient_next").find('a').attr('data-dt-idx',after);

		$("#viewClient_previous").attr('class','paginate_button previous');
		if(currentPage == 1){
			$("#viewClient_previous").attr('class','paginate_button previous disabled');
		}
		$("#viewClient_next").attr('class','paginate_button next');
		if(currentPage == a){
			$("#viewClient_next").attr('class','paginate_button next disabled');
		}
		for (var i = 0; i<a; i++){
			var text = '<li class="paginate_button"><a href="#" aria-controls="tableLog" data-dt-idx="'+(i+1)+'" tabindex="0">'+(i+1)+'</a></li>';
			var elipsisLeft = '<li class="paginate_button disabled" id="tableLog_ellipsis"><a href="#" aria-controls="tableLog" data-dt-idx="2" tabindex="0">…</a></li>';
			var elipsisRight = '<li class="paginate_button disabled" id="tableLog_ellipsis"><a href="#" aria-controls="tableLog" data-dt-idx="" tabindex="0">…</a></li>';
			if ((i+1)==currentPage){
				text = '<li class="paginate_button active"><a href="#" aria-controls="tableLog" data-dt-idx="'+(i+1)+'" tabindex="0">'+(i+1)+'</a></li>';
			}
			$("#viewClient_next").before(text);
			if (((currentPage-1)>3)&&(i==0)){ 
				$("#viewClient_next").before(elipsisLeft);
				var q2=a-4;
				if(q2<currentPage){
					console.log('here');
					for(var j=q2;j<currentPage-1;j++){
						console.log('here');
						var text = '<li class="paginate_button"><a href="#" aria-controls="tableLog" data-dt-idx="'+j+'" tabindex="0">'+j+'</a></li>';
						$("#viewClient_next").before(text);
					}
				}
				i = currentPage-3;

			}
			if (((a-currentPage)>3)&&(i==currentPage)){

				if(i<4){

					for(var j=i+1;j<=4;j++){
						
						var text = '<li class="paginate_button"><a href="#" aria-controls="tableLog" data-dt-idx="'+(j+1)+'" tabindex="0">'+(j+1)+'</a></li>';
						$("#viewClient_next").before(text);
					}
				}
				$("#viewClient_next").before(elipsisRight);
				i = a-2;
			}
			clearContainer();
		}
		var parseLength = dataLength;
		if(dataLength>dataClient.length){
			parseLength = dataClient.length;
		}
		for(var i = (currentPage-1)*dataLength; i<(currentPage*parseLength); i++){
			parseDataClient(dataClient[i]);
		}
	}
	$("#viewClientPagging").on('click','a',function(e){
		e.preventDefault();
		currentPage = $(this).attr('data-dt-idx');
		pagging();
	});
	$("disabled").on('click','a',function(e){
		e.preventDefault();
	});
	function getCount(){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'countElement',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					$("#countClient").html(data.client);
					$("#countInvoice").html(data.invoice);
					$("#countKerja").html(data.kerja);
					$("#countConfirm").html(data.confirm);
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}	
	function parseDataClient(data){
		$('#item').find('#itemName').html(data.nama);
		$('#item').find('#itemCount').html(data.countKerja);
		$('#item').find('#itemEdit').attr('data-id',data.id);
		$('#item').find('#itemDelete').attr('data-id',data.id);
		$('#item').find('#idContainer').attr('data-id',data.id);
		$('#viewClientContainer').append($("#item").html());
	}
	function clearContainer(){
		$('#viewClientContainer').html('');
	}
	function setValuePicker(){
		var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'listPekerjaan',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					$('#selectorPekerjaan').html('<option value="0">Filter sesuai pekerjaan</option>');
					$.each(data,function(i,item){
						$('#selectorPekerjaan').append('<option value="'+item.id+'" >'+item.nama+'</option>')
					});						
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	$('#selectorPekerjaan').on('change',function(){
		var id = $('#selectorPekerjaan').val();
		var keyword = $('#keyword').val();
		filterClient(id,keyword);
	});
	function filterClient(id,keyword){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'filterClient',
				data:{'id':id,'keyword':keyword},
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					dataClient = data;
					clearContainer();
					currentPage = 1;
					pagging();										
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});	
	}
	$('#keyword').on('keyup',function(){
		var id = $('#selectorPekerjaan').val();
		var keyword = $('#keyword').val();
		filterClient(id,keyword);	
	});
	function loadCheckPekerjaan(){
		var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'listPekerjaan',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					$('#checkContainer').html('');
					$.each(data,function(i,item){
						var text =  '<div class="form-group col-xs-3">'+
									'<label class="checkbox-inline">'+
									'<input type="checkbox" value="'+item.id+'" id="check_'+item.id+'" name="pekerjaan[]">'+item.nama+
									'</label></div>'
						$('#checkContainer').append(text);
					});						
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}

	$('#manipulationForm').submit(function(e) {
		if (!validationPekerjaan()){
			alert('Anda harus memilih pekerjaan setidaknya 1');
		} else if (logoValidation()) {
			var baselink = $('#baselink').val();
		    $.ajax( {
		      url: $(this).prop('action'),
		      type: 'POST',
		      data: new FormData( this ),
		      processData: false,
		      contentType: false,
		      success: function(response){
		      	var data = jQuery.parseJSON(response)
		      	if (data.status == "success"){
		      		alert("Data klien berhasil disimpan");
		      		clearForm();
		      		pageTransition(1);
		      		getListClient();
		      	} else {
		      		alert("Error");
		      	}
		      }
		    });
		}
	    e.preventDefault();
	  } );
	function validationPekerjaan(){
		var x = false;	
		$('input[type=checkbox]').each(function(i,obj){
			if($(this).prop('checked')==true){x = true}
		});
		return x;
	}
	function logoValidation(){
		if ($('#inputLogo')[0].files.length == 1){
			var fsize = $('#inputLogo')[0].files[0].size; //get file size
			var ftype = $('#inputLogo')[0].files[0].type; // get file type
			switch(ftype) {
		        case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		   	    break;
		    default:
		       	alert('Jenis file logo berbeda! File ini tidak bisa didaftarkan');
				return false
			}
			if(fsize>1048576) {
				alert('File gambar terlalu besar!');
				return false
			}
			return true
		} else {
			return true
		}	
	}
	function clearForm(){
		$('input[type="text"]').val('');
		$('input[type="email"]').val('');
		$('textarea').val('');
		$('input[type="file"]').val('');
		$('input[type="checkbox"]').prop('checked',false);
	}
	function pageTransition(type){
		if (type == 1){
			$('#manipulationPanel').slideUp();
			$('#dataList').slideDown();
		} else {
			$('#manipulationPanel').slideDown();
			$('#dataList').slideUp();
		}
	}

	$('#viewClientContainer').on('click','.item',function(e) { 
			var url = baselink+"pekerjaanKlien/"+ $(this).attr('data-id');
			//$.redirect(url, {'idPekerjanKlien': $(this).attr('data-id')});
			window.location.href=url;
	});
	$('#viewClientContainer').on('click','a',function(event) {
	    var id = $(this).attr('id');
	    var dataId = $(this).attr('data-id');
		 if (id == 'itemEdit') {
	        event.preventDefault();
	        pageTransition(0);
	        $("#formTitle").html("Ubah Data Klien");
	    	$("#manipulationForm").prop('action',baselink+'editClient');
	        detailClient(dataId);
	     } else if (id == 'itemDelete'){
	     	event.preventDefault();
	     	$("#modal_delete").modal('show');
	     	$("#btn_modal").attr('data-id',dataId);
	     } else {
	            //redirect
	 	 }
	});
	$('#viewClientContainer').on('click','#addClient',function(e) { 
	   $("#formTitle").html("Tambah Data Klien");
	   $("#manipulationForm").prop('action',baselink+'addClient'); 
	   pageTransition(0);
	});


	$('#btn_cancel').click(function(){
		clearForm();
		pageTransition(1);
	});
	function detailClient(id){
		var baselink = $('#baselink').val();
			$.ajax( {
		      url: baselink+'detailClient',
		      data:{'id':id},
		      type: 'POST',
		      cache: false,
		      success: function(response){
		      	var data = jQuery.parseJSON(response);
		      	parseData(data);
		      }
		    });	
	}
	function parseData(data){
		$("#idClientInput").val(data[0].id_client);
		$("#inputNama").val(data[0].nama_client);
		$("#inputJenis").val(data[0].jenis_perusahaan);
		$("#inputAlamat").val(data[0].alamat);
		$("#inputPhone").val(data[0].no_telepon);
		$("#inputEmail").val(data[0].email);
		$("#startContract").val(data[0].start_contract);
		$("#inputDeskripsi").val(data[0].deskripsi);
		$("#endContract").val(data[0].end_contract);
		var listPekerjaan = data[0].list_kerja.split(',');
		$.each(listPekerjaan,function(i,item){
			$('#check_'+item).prop('checked',true);
		})
	}

	$("#btn_modal").click(function(){
		var id = $(this).attr('data-id');
		$.ajax( {
		      url: baselink+'deleteClient',
		      data:{'id':id},
		      type: 'POST',
		      cache: false,
		      success: function(response){
		      	$("#modal_delete").modal("hide");
		      	var data = jQuery.parseJSON(response);
		      	if (data.status == "success"){
		      		alert("Data berhasil dihapus");
		      		getListClient();
		      	} else {
		      		alert("Data gagal dihapus");
		      	}
		      }
		    });	
	});
    function loadData(){
		$.ajax({
			type: "POST",
			url: baselink+'log/currentLog',
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$.each(data,function(i,item){
					addItemList(item,count);
					count++;
				});
				$('#tableLog').DataTable({
			      'paging'      : true,
			      'lengthChange': false,
			      'searching'   : false,
			      'ordering'    : false,
			      'info'        : false,
			      'autoWidth'   : false
			    });
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});    	
    }
    function addItemList(data,count){
    	var text = '<tr>'+
	             		'<td>'+data.activity+'</td>'+
	             		'<td><small class="label label-danger"><i class="fa fa-clock-o"></i>&nbsp;'+data.date+'</small></td>'+
					'</tr>';
    	$("#listView").append(text);
    }
});
