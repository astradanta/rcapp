$(document).ready(function(){
	var baselink = $("#baselink").val();
	listUser();

	function listUser(){
		$.ajax({
			type: "POST",
			url: baselink+'user/listUser',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$("#listView").html('');
				$.each(data,function(i,item){
					parseListUser(item,count);
					count++;
				});
				$("#userTable").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});  
	}

	function parseListUser(data,count){
		$('#actionItemTemplate').find('#editList').attr("data-id",data.id_user);
		$('#actionItemTemplate').find('#deleteList').attr("data-id",data.id_user);
		$('#actionItemTemplate').find('#detailList').attr("data-id",data.id_user);
		var text = 	'<tr>'+
					'<td>'+count+'</td>'+
					'<td>'+data.nama+'</td>'+
					'<td>'+data.email+'</td>'+
					'<td>'+data.jenis_user+'</td>'+
					'<td style="display:flex">'+
						$("#actionItemTemplate").html()+
					'</td>'+
					'</tr>';
		$("#listView").append(text);
	}

	$('.btnGenerate').click(function(){
		var target = $(this).attr('data-target');
		$.ajax({
			type: "POST",
			url: baselink+'user/generatePassword',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				$("#"+target).html("Password : ");
				$("#"+target).after("<label class='control-label'>"+response+"</label>")
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	});
	$("#addForm").submit(function(e){
		e.preventDefault();
		if ($("#addPassword").val()!=$("#addRetype").val()){
			alert("Password tidak sama");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'user/addUser',
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("User berhasil ditambahkan");
						clearForm();
						$("#userTable").DataTable().destroy();
						listUser();
						pageTransition(1);
					}else{
						alert("Gagal menambahkan user");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});
	function clearForm(){
		$('input[type="text"]').val('');
		$('input[type="email"]').val('');
		$('input[type="password"]').val('');
		$("#addJenis").val($("#addJenis option:first").val());
	}
	$("#listView").on('click','#editList',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});
	$("#listView").on('click','#detailList',function(){
		var id = $(this).attr('data-id');
		getDetail(id,1);
	});		
	function getDetail(id,isDetail = 0){
		$.ajax({
			type: "POST",
			url: baselink+'user/detailUser',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
			    console.log(response);
				var data = jQuery.parseJSON(response);
				parseDetail(data[0],isDetail);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data,isDetail = 0){
		console.log(data);
		if (isDetail == 0){
			if(data.jenis_user == 0){$("#editJenis").prop('disabled',true)}	else {
				$("#editJenis").prop('disabled',false);
			}	
			$("#editName").val(data.nama);
			$("#editEmail").val(data.email);
			$("#editPassword").val(data.password);
			$("#editRetype").val(data.password);
			$("#editIdUser").val(data.id_user);
			$("#editJenis option[value="+data.jenis_user+"]").prop("selected",true);
			$("#editModal").modal("show");
		} else {
			$("#detailName").val(data.nama);
			$("#detailEmail").val(data.email);
			$("#detailPassword").val(data.password);
			if(data.jenis_user == 0){$("#detailJenis").val("Klien");}
			else{$("#detailJenis").val(data.role_name);}
			$("#detailTipe").val(data.tipe);
			$("#detailCreateAt").val(data.created_at);
			$("#detailUpdateAt").val(data.updated_at);
			$("#detailModal").modal("show");
		}
	}
	$("#editForm").submit(function(e){
		e.preventDefault();
		if ($("#editPassword").val()!=$("#editRetype").val()){
			alert("Password tidak sama");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'user/editUser',
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data User berhasil dirubah");
						clearForm();
						$("#editModal").modal("hide");
						$("#userTable").DataTable().destroy();
						listUser();
					}else{
						alert("Gagal merubah data user");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});	
		}
	});
	$("#listView").on('click','#deleteList',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'user/deleteUser',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Datat User Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#userTable").DataTable().destroy();
					listUser()
				} else {
					alert("Gagal menghapus data user")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	function pageTransition(status){
		if(status == 0){
			$("#addPanel").slideDown();
			$("#viewPanel").slideUp();
		} else {
			$("#addPanel").slideUp();
			$("#viewPanel").slideDown();
		}
	}
	$("#btn_close").click(function(){
		pageTransition(1);
	});
	$("#btn_add").click(function(){
		pageTransition(0);
	})

});