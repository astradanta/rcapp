$( document ).ready(function() {
	var baselink = $('#baselink').val();
	countInvoice();
	getListPekerjaan();
	setInterval(function(){ 
		countInvoice();
	
	}, 5000);
	var idPekerjaanKlien = $("#idPekerjaan").val();
	function countInvoice(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/new',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){				
				var data = jQuery.parseJSON(response);
				if (data.length > 0){
					$("#invoiceCount").html(data.length);
				} else {
					$("#invoiceCount").html('');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		       	console.log(xhr.status);
		       	console.log(xhr.responseText);
		       	console.log(thrownError);
			}
		});			
	}
	function getListPekerjaan(){
		$.ajax({
			type: "POST",
			url: baselink+'listPekerjaan',
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				$("#viewList").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		       	console.log(xhr.status);
		       	console.log(xhr.responseText);
		       	console.log(thrownError);
			}
		});		
	}


	function parseList(data){
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find("#itemLeftIcon").html('<i style="font-size: 40px;">'+persentase+'<sup>%</sup></i>')
		$("#listItemTemplate").find("#itemContainer").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan);		
		$("#listItemTemplate").find("#editItem").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#deleteItem").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").html(data.deadline);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#namaPekerjaan").html(data.nama_pekerjaan);
		$("#viewList").append($("#listItemTemplate").html());
	}
	// $("#viewList").on("click","#itemContainer",function(){
	// 	var url = baselink+'pekerjaan'
	// 	$.redirect(url, {'id_client': $(this).attr('data-client'),'id_pekerjaan':$(this).attr('data-pekerjaan')});			
	// });
	$("#invoicePanel").on('click',function(){
		window.location.href = baselink+'invoice';
	});
	$("#confirmPanel").on('click',function(){
		window.location.href = baselink+'confirm';
	});	
	$("#saranPanel").on('click',function(){
		window.location.href = baselink+'saran';
	});	
	$('#viewList').on('click','#itemContainer',function(e) { 
			var url = baselink+'pekerjaan/'+$(this).attr('data-id')+'/'+idPekerjaanKlien+'/'+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#viewList').on('click','#itemlistDeadline',function(e) { 
			var url = baselink+'pekerjaan/'+$(this).attr('data-id')+'/'+idPekerjaanKlien+'/'+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});	
	$('#viewList').on('click','#itemLeftIcon',function(e) { 
			var url = baselink+'pekerjaan/'+$(this).attr('data-id')+'/'+idPekerjaanKlien+'/'+$(this).attr('data-pekerjaan');
			window.location.href = url;
	});		
});