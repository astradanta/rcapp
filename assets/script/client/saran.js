$(document).ready(function(){
	var baselink = $("#baselink").val();
	 CKEDITOR.replace('editor1');
	 CKEDITOR.replace('editor2');
	list();
	function list(){
		CKEDITOR.instances.editor1.setData("");
		$.ajax({
			type: "POST",
			url: baselink+'saran/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				$("#listSaran").html('');
				parseList(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
		if (data.count == 0){
			$("#listSaran").html($("#listNoItem").html());
		} else {
			$("#listWithItem").html("");
			$.each(data.data,function(i,item){
				$("#itemTemplate").find("#tanggalBuat").html('Di inputkan pada : '+item.create_at);
				$("#itemTemplate").find("#tanggalUbah").html('Diubah pada : '+item.update_at);
				$("#itemTemplate").find("#contentSaran").html(item.content);
				$("#itemTemplate").find("#editItem").attr('data-id',item.id_saran);				
				$("#itemTemplate").find("#deleteItem").attr('data-id',item.id_saran);
				$("#itemTemplate").find("#editItem").attr('data-target',"contentSaran"+item.id_saran);					
				$("#listWithItem").append($("#itemTemplate").html());
				$("#listWithItem").find("#contentSaran").attr('id',"contentSaran"+item.id_saran);		
			});
			$("#listSaran").html($("#listWithItem").html());
		}
	}
	$("#listSaran").on('click','#deleteItem',function(e){
		e.preventDefault()
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#listSaran").on('click','#editItem',function(e){
		e.preventDefault()
		var id = $(this).attr('data-id');
		$("#manipulateModal").modal("show");
		var target = $(this).attr('data-target');
		var data = $("#"+target).html();
		CKEDITOR.instances.editor2.setData(data);
		$("#idDetail").val($(this).attr('data-id'));
	});	
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	CKEDITOR.instances.editor2.setData("");
	});		
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'saran/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		var content = CKEDITOR.instances.editor2.getData();
		CKEDITOR.instances.editor2.updateElement();
		if(content == ""){
			alert("isi saran tidak boleh kosong");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data saran/masukan disimpan");
						$("#manipulateModal").modal("hide");
						list();
					}else{
						alert("Gagal menyimpan data saran/masukan");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});				
		}
		
	});
	$("#addForm").submit(function(e){
		e.preventDefault();
		var content = CKEDITOR.instances.editor1.getData();
		CKEDITOR.instances.editor1.updateElement();
		if(content == ""){
			alert("isi saran tidak boleh kosong");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data saran/masukan disimpan");
						list();
					}else{
						alert("Gagal menyimpan data saran/masukan");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});				
		}		
	});
});