$(document).ready(function(){
	var baselink = $("#baselink").val();
	getOnProgress();
	getArchive();

	function getOnProgress(){
		var id_pekerjaan = $("#idPekerjaan").val();
		var id_client = $("#idClient").val();
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/getOnProgress',
			data:{"id_client":id_client,"id_pekerjaan":id_pekerjaan},
			cache: false,
			success: function(response){
				//console.log(response);
				var data = jQuery.parseJSON(response);
				parseDataOnProgress(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function getArchive(){
		var id_pekerjaan = $("#idPekerjaan").val();
		var id_client = $("#idClient").val();
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/getArchive',
			data:{"id_client":id_client,"id_pekerjaan":id_pekerjaan},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				parseDataArchive(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}	
	function parseDataOnProgress(data){
		if(data.count == 0){
			$("#contentOnProgress").html($("#noData").html());
		} else {
			$("#withData").find("#containerSumber").html("");
			$("#withData").find("#containerPekerjaan").html("");
			$("#contentOnProgress").html('');
			$.each(data.itemData,function(i,item){
				if(item.jenis_pekerjaan==0){
					if (item.upload_file == null){
						$("#sumberNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#sumberNoItem").find("#uploadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#sumberNoItem").find("#uploadButton").attr("data-kerja",data.id_kerja);
						$("#containerSumber").append($("#sumberNoItem").html());
					} else {
						$("#sumberWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#sumberWithItem").find("#editButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#sumberWithItem").find("#editButton").attr("data-kerja",data.id_kerja);
						$("#sumberWithItem").find("#editButton").attr("data-url",item.upload_file);
						$("#sumberWithItem").find("#editButton").attr("data-id",item.id_detail_kerja);
						$("#containerSumber").append($("#sumberWithItem").html());
					}
				} else {
					if (item.upload_file == null){
						$("#pekerjaanNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#containerPekerjaan").append($("#pekerjaanNoItem").html());
					} else {
						$("#pekerjaanWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-kerja",data.id_kerja);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-url",item.upload_file);
						$("#containerPekerjaan").append($("#pekerjaanWithItem").html());
					}
				}
			});
			$("#withData").find($("#lblStatus").html("Status : "+data.status));
			$("#withData").find($("#lblDeadline").html("Deadline : "+data.deadline));
			$("#withData").find($("#lblStartdate").html("Start Date : "+data.start_date));
			$("#contentOnProgress").html($("#withData").html());
		}
	}
	function parseDataArchive(data){
		if(data.count == 0){
			$("#archiveContainer").html($("#archiveNoItem").html());
		} else {
			$("#archiveWithItem").find("#listView").html('');
			$.each(data.itemData,function(i,item){
				var txt = '<tr>'+
							'<td>'+(i+1)+'</td>'+
							'<td>'+item.nama_detail_pekerjaan+'</td>'+
							'<td>'+item.created_at+'</td>'+
							'<td>'+item.updated_at+'</td>'+
							'<td>'+item.updated_by+'</td>'+
							'<td><button type="button" class="btn btn-primary" id="archiveDownload" data-url="'+item.upload_file+'">Download</button></td>'+
						'</tr>';
               $("#archiveWithItem").find("#listView").append(txt);
			});
			$("#archiveContainer").html($("#archiveWithItem").html());
		}
	}
	$("#contentOnProgress").on('click','#uploadButton',function(){
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#manipulateModal").modal("show");		
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data berhasil disimpan");
						$("#manipulateModal").modal("hide");
						getOnProgress();
					}else{
						alert("Gagal menyimpan data");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});
		
	});	
	$("#contentOnProgress").on('click','#editButton',function(){
		var id = $(this).attr('data-id');
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		var url = $(this).attr('data-url');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#idDetail").val(id);
		$("#manipulateForm").prop('action',baselink+"pekerjaan/edit");
		$("#inputUrl").val(url);
		$("#manipulateModal").modal("show");		
	});	
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	$("#inputUrl").val("");
	  	$("#manipulateForm").prop('action',baselink+"pekerjaan/add");
	});
	$("#contentOnProgress").on('click','#downloadButton',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#archiveContainer").on('click','#archiveDownload',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});
});
