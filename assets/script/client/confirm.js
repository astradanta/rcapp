$(document).ready(function(){
	var baselink = $("#baselink").val();
	selectInvoice();
	list();
	listReceipt();
	function selectInvoice(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/new',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#selectPicker").html('');
				$("#selectPicker").append('<option value="0">Pilih invoice</option>')
				$.each(data,function(i,item){
					$("#selectPicker").append('<option value="'+item.id_invoice+'">'+item.no_invoice+'</option>')
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		 if ($("#selectPicker").val()==0){
			alert("Anda harus memilih invoice");
		} else if(!inputBukti($(this).prop('action'))){
			alert("Anda harus memasukan bukti pembayaran");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data konfirmasi pembayaran berhasil disimpan");
						$("#manipulateModal").modal("hide");
						$("#tableInvoice").DataTable().destroy();
						list();
						selectInvoice();
					}else{
						alert("Gagal menyimpan data konfirmasi pembayaran");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});
	function inputBukti(str){
		if ($('#inputBukti')[0].files.length == 1){
			var fsize = $('#inputBukti')[0].files[0].size; //get file size
			var ftype = $('#inputBukti')[0].files[0].type; // get file type
			switch(ftype) {
		        case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
		   	    break;
		    default:
		       	alert('Jenis file logo berbeda! File ini tidak bisa didaftarkan');
				return false
			}
			if(fsize>1048576) {
				alert('File gambar terlalu besar!');
				return false
			}
			return true
		} else {
			if(str==(baselink+'confirm/add')){
				return false
			} else {
				return true
			}
		}	
	}	
	function resetModal(){
		$('input[type="text"]').val('');
		$('textarea').val('');
		$("#selectPicker").val($("#selectPicker option:first").val());
		$("#manipulateForm").prop('action',baselink+'confirm/add');
		$("#modalTitle").html("Konfirmasi Pembayaran");
		$("#selectPicker option[value=0]").html("Pilih invoice");
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	});
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'confirm/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({
					'autoWidth'   : true,
      				'scrollX':true
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.nama+'</td>'+
						'<td>'+data.nama_bank+'</td>'+
						'<td>'+data.atas_nama_bank+'</td>'+
						'<td>'+data.status_konfirmasi+'</td>'+
						'<td>'+data.note+'</td>'+
						'<td style="text-align:center;">';
							if(data.status_konfirmasi == "Menunggu verifikasi"){
								text = text+'<button data-id="'+data.id_konfirmasi+'" class="btn btn-warning " id="editItem" type="button"><i class="fa fa-pencil"></i></button>'+
										'<button data-id="'+data.id_konfirmasi+'" class="btn btn-info " id="viewItem" data-url="'+data.file_bukti+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-download"></i></button>'+
										'<button data-id="'+data.id_konfirmasi+'" class="btn btn-danger " id="deleteItem" type="button" ><i class="fa fa-trash"></i></button>';
							} else {
								text = text+'<button data-id="'+data.id_konfirmasi+'" class="btn btn-info " id="viewItem" data-url="'+data.file_bukti+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-download"></i>&nbsp;Download</button>';
							}
							
						text = text +		
								
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#listView").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#listView").on('click','#editItem',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});	
	function getDetail(id){
		$.ajax({
			type: "POST",
			url: baselink+'confirm/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				parseDetail(data[0]);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data){		
		$("#selectPicker option[value=0]").html(data.no_invoice);
		$("#selectPicker option[value=0]").val(data.id_invoice);
		$("#inputNamaBank").val(data.nama_bank);
		$("#inputAtasNamaBank").val(data.atas_nama_bank);		
		$("#inputNote").val(data.note);		
		$("#inputBukti").val(data.file_bukti);
		$("#idDetail").val(data.id_konfirmasi);
		$("#manipulateModal").modal("show");
		$("#manipulateForm").prop('action',baselink+'confirm/edit');
		$("#modalTitle").html("Ubah Data Konfirmasi Pembayaran");		
	}	
	$("#listView").on('click','#deleteItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'confirm/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list();
					selectInvoice();
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});	
	function listReceipt(){
		$.ajax({
			type: "POST",
			url: baselink+'confirm/listReceipt',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);

				$("#listViewReceipt").html('');
				$.each(data,function(i,item){
					parseListReceipt(item);
				});
				$("#tableReceipt").DataTable({
					'autoWidth'   : false,
      				'scrollX':false
				});
				$('#tableReceipt').DataTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseListReceipt(data){
		var text = 	'<tr>'+
						'<td>'+data.nama+'</td>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.deskripsi+'</td>'+
						'<td>'+data.created_at+'</td>'+
						'<td>'+data.updated_at+'</td>'+						
						'<td style="text-align:center;">'+
							'<button data-id="'+data.id_receipt+'" class="btn btn-info " id="viewItem" data-url="'+data.file_receipt+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-download"></i>&nbsp;Download</button>'+							
						'</td>'+
					'</tr>';
		$("#listViewReceipt").append(text);
	}
	$("#listViewReceipt").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});		
});