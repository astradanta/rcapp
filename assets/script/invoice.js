$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	getListClient();
	archive();
	$('#inputTanggal').datepicker({
	   autoclose: true
	});
	$('#inputDue').datepicker({
	   autoclose: true
	})			
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.tanggal_invoice+'</td>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.nama_client+'</td>'+
						'<td>'+data.due_date+'</td>'+
						'<td>'+data.total+'</td>'+
						'<td>'+data.note+'</td>'+
						'<td>'+data.status+'</td>'+
						'<td style="text-align:center;">'+
							'<button data-id="'+data.id_invoice+'" class="btn btn-default col-md-12" id="cancelItem" style="margin-bottom:3px" type="button">Cancel</button>'+
							'<button data-id="'+data.id_invoice+'" class="btn btn-warning " id="editItem" type="button"><i class="fa fa-pencil"></i></button>'+
							'<button data-id="'+data.id_invoice+'" class="btn btn-info " id="viewItem" data-url="'+data.file_invoice+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-eye"></i></button>'+
							'<button data-id="'+data.id_invoice+'" class="btn btn-danger " id="deleteItem" type="button"><i class="fa fa-trash"></i></button>'+
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#btnAddInvoice").click(function(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/getNoInvoice',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
			    console.log(response);
				var data = jQuery.parseJSON(response);
				$("#no_invoice").html(data.no_invoice);
				$("#input_no_invoice").val(data.no_invoice);
				$("#manipulateModal").modal("show");
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	function getListClient(){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'listClient',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);

					$.each(data,function(i,item){
						$("#selectPicker").append('<option value="'+item.id+'">'+item.nama+'</option>');
					});								
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		 if ($("#selectPicker").val()==0){
			alert("Anda harus memilih klien");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data invoice berhasil disimpan");
						$("#manipulateModal").modal("hide");
						$("#tableInvoice").DataTable().destroy();
						list();
					}else{
						alert("Gagal menyimpan data invoice");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});	
	function resetModal(){
		$('input[type="text"]').val('');
		$('textarea').val('');
		$("#selectPicker").val($("#selectPicker option:first").val());
		$("#manipulateForm").prop('action',baselink+'invoice/add');
		$("#modalTitle").html("Tambah Data Invoice");
	}
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	});
	$("#listView").on('click','#editItem',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});	
	function getDetail(id){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				parseDetail(data[0]);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data){
		$("#no_invoice").html(data.no_invoice);
		$("#input_no_invoice").val(data.no_invoice);		
		$("#selectPicker option[value="+data.id_client+"]").prop("selected",true);
		$("#inputTanggal").val(data.tanggal_invoice);
		$("#inputDue").val(data.due_date);		
		$("#idDetail").val(data.id_invoice);		
		$("#inputTotal").val(data.total);
		$("#inputUrl").val(data.file_invoice);
		$("#inputNote").val(data.note);
		$("#manipulateModal").modal("show");
		$("#manipulateForm").prop('action',baselink+'invoice/edit');
		$("#modalTitle").html("Ubah Data Invoice");		
	}
	$("#listView").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#listView").on('click','#deleteItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'invoice/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});	
	$("#listView").on('click','#cancelItem',function(){
		var id = $(this).attr('data-id');
		$("#modal_cancel").modal("show");
		$("#btn_cancel").attr('data-id',id);
	});
	$("#btn_cancel").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'invoice/cancel',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Berhasil membatalkan invoice");
					$("#modal_cancel").modal("hide");
					$("#tableInvoice").DataTable().destroy();
					list()
				} else {
					alert("Gagal membatalkan invoice")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});
	function archive(){
		$.ajax({
			type: "POST",
			url: baselink+'invoice/archive',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response)
				var data = jQuery.parseJSON(response);

				$("#listViewArchive").html('');
				$.each(data,function(i,item){
					parseArchive(item);
				});
				$("#tableArchive").DataTable({scrollX:false});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseArchive(data){
		var text = 	'<tr>'+
						'<td>'+data.tanggal_invoice+'</td>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.nama_client+'</td>'+						
						'<td>'+data.total+'</td>'+
						'<td>'+data.note+'</td>'+
						'<td>'+data.status+'</td>'+
						'<td style="text-align:center;">'+							
							'<button data-id="'+data.id_invoice+'" class="btn btn-info " id="viewItem" data-url="'+data.file_invoice+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-eye"></i> View</button>'+							
						'</td>'+
					'</tr>';
		$("#listViewArchive").append(text);
	}
	$("#listViewArchive").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});										
});