$(document).ready(function(){
	var baselink = $("#baselink").val();
	list();
	function list(){
		$.ajax({
			type: "POST",
			url: baselink+'confirm/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);

				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
				$("#tableInvoice").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseList(data){
		var text = 	'<tr>'+
						'<td>'+data.no_invoice+'</td>'+
						'<td>'+data.nama_client+'</td>'+
						'<td>'+data.nama_bank+'</td>'+
						'<td>'+data.atas_nama_bank+'</td>'+
						'<td>'+data.status_konfirmasi+'</td>'+
						'<td>'+data.note+'</td>'+
						'<td style="text-align:center;">'+
							'<button data-id="'+data.id_konfirmasi+'" class="btn btn-info " id="viewItem" data-url="'+data.file_bukti+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-download"></i></button>'+					
							'<button data-id="'+data.id_konfirmasi+'" class="btn btn-warning " id="editItem" data-status="'+data.status_konfirmasi+'" type="button" style="margin-left:3px;margin-right:3px;"><i class="fa fa-pencil"></i>&nbsp;Status</button>'+					
						'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#listView").on('click','#viewItem',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#listView").on('click','#editItem',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-status');
		$("#manipulateModal").modal("show");
		$("#changeButton").attr('data-id',id);
	});	
	$("#changeButton").click(function(){
		var id = $(this).attr('data-id');
		if ($("input[name=statusKonfirmasi]:checked").val() == undefined){
			alert("Anda harus memilih salah satu status");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'confirm/changeStatus',
				data:{"id":id,"status":$("input[name=statusKonfirmasi]:checked").val()},
				cache: false,
				success: function(response){
					console.log(response)
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Berhasil mengubah status konfirmasi");
						$("#manipulateModal").modal("hide");
						$("#tableInvoice").DataTable().destroy();
						list();
					} else {
						alert("Gagal mengubah status konfirmasi");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}

	});				
});