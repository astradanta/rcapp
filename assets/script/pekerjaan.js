$(document).ready(function(){
	var baselink = $("#baselink").val();
	$("#addSelect").html($("#taxUser").html());
	listPekerjaan();
	function listPekerjaan(){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/listPekerjaan',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				var count = 1;
				$("#listView").html('');
				$.each(data,function(i,item){
					parseListUser(item,count);
					count++;
				});
				$("#pekerjaanTable").DataTable({scrollX:true});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});  
	}
	function parseListUser(data,count){
		$('#actionItemTemplate').find('#editList').attr("data-id",data.id_pekerjaan);
		$('#actionItemTemplate').find('#deleteList').attr("data-id",data.id_pekerjaan);
		$('#actionItemTemplate').find('#detailList').attr("data-id",data.id_pekerjaan);
		var text = 	'<tr>'+
					'<td>'+count+'</td>'+
					'<td>'+data.nama_pekerjaan+'</td>'+
					'<td>'+data.deskripsi_pekerjaan+'</td>'+				
					'<td>'+data.kategori+'</td>'+
					'<td>'+data.tipe+'</td>'+
					'<td style="display:flex;">'+
						$("#actionItemTemplate").html()+
					'</td>'+
					'</tr>';
		$("#listView").append(text);
	}
	$("#type1").click(function(){
		$("#addSelect").html($("#taxUser").html());
	});
	$("#type2").click(function(){
		$("#addSelect").html($("#financeUser").html());
	});
	$("#addForm").submit(function(e){
		e.preventDefault();
		if ($("#addDeskripsi").val()==""){
			alert("Deskripsi harus diisi");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'pekerjaan/addPekerjaan',
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Pekerjaan berhasil ditambahkan");
						clearForm();
						$("#pekerjaanTable").DataTable().destroy();
						listPekerjaan();
						pageTransition(1);
					}else{
						alert("Gagal menambahkan pekerjaan");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});
	function clearForm(){
		$('input[type="text"]').val('');
		$('textarea').val('');
		$("#type1").prop('checked',true);
		$("#kategori1").prop('checked',true);
		$("#addSelect").html($("#taxUser").html());
	}
	$("#listView").on('click','#editList',function(){
		var id = $(this).attr('data-id');
		getDetail(id);
	});
	$("#listView").on('click','#detailList',function(){
		var id = $(this).attr('data-id');
		getDetail(id,1);
	});	
	function getDetail(id,isdetail = 0){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/detailPekerjaan',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseDetail(data[0],isdetail);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	function parseDetail(data,isdetail = 0){

		if(isdetail==0){
				$("#editName").val(data.nama_pekerjaan);
				$("#editDeskripsi").val(data.deskripsi_pekerjaan);
				$("#editIdPekerjaan").val(data.id_pekerjaan);
				if(data.kategori == "Reguler") {
					$("#kategori2Edit").prop('checked',true);
				} else {
					$("#kategori1Edit").prop('checked',true);
				}
				if(data.tipe == "Finance") {
					$("#tipe2Edit").prop('checked',true);
					$("#editSelect").html($("#financeUser").html());
				} else {
					$("#tipe1Edit").prop('checked',true);
					$("#editSelect").html($("#taxUser").html());
				}
				$("#editSelect option[value="+data.id_user+"]").prop("selected",true);
				$("#editModal").modal("show");
		} else {
			$("#detailName").val(data.nama_pekerjaan);
			$("#detailDeskripsi").val(data.deskripsi_pekerjaan);
			$("#detailKategori").val(data.kategori);
			$("#detailTipe").val(data.tipe);
			$("#detailCreateAt").val(data.created_at);
			$("#detailUpdateAt").val(data.updated_at);
			$("#detailUpdateBy").val(data.updated_by);
			$("#detailModal").modal("show");
		}
	}
	$("#editForm").submit(function(e){
		e.preventDefault();
		if ($("#editDeskripsi").val()==""){
			alert("Deskripsi harus diisi");
		} else {
			$.ajax({
				type: "POST",
				url: baselink+'pekerjaan/editPekerjaan',
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){

					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data Pekerjaan berhasil dirubah");
						clearForm();
						$("#editModal").modal("hide");
						$("#pekerjaanTable").DataTable().destroy();
						listPekerjaan();
					}else{
						alert("Gagal merubah data Pekerjaan");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});	
		}
	});	
	$("#listView").on('click','#deleteList',function(){
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaan/deletePekerjaan',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Datat Pekerjaan Berhasil dihapus");
					$("#modal_delete").modal("hide");
					$("#pekerjaanTable").DataTable().destroy();
					listPekerjaan()
				} else {
					alert("Gagal menghapus data Pekerjaan")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});	
	function pageTransition(status){
		if(status == 0){
			$("#addPanel").slideDown();
			$("#viewPanel").slideUp();
		} else {
			$("#addPanel").slideUp();
			$("#viewPanel").slideDown();
		}
	}
	$("#btn_close").click(function(){
		pageTransition(1);
	});
	$("#btn_add").click(function(){
		pageTransition(0);
	})									
});