$(document).ready(function(){
	var baselink = $("#baselink").val();
	getOnProgress();
	var id_client = $("#id_client").val();
	var id_pekerjaan = $("#id_pekerjaan").val();
	var count = 0;
	var itemCount = 0;

	function getOnProgress(){
		$.ajax({
			type: "POST",
			url: baselink+'client/pekerjaan/getArchive',
			data:{"id_client":$("#id_client").val(),"id_pekerjaan":$("#id_pekerjaan").val()},
			cache: false,
			success: function(response){
				console.log(response);
				console.log(id_client+" "+id_pekerjaan)
				var data = jQuery.parseJSON(response);
				parseDataOnProgress(data);

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseDataOnProgress(data){
		var dataCount = 0;
		if(data.count == 0){
			$("#contentOnProgress").html($("#noData").html());
		} else {
			$("#changeStatus").attr('data-id',data.id_kerja);
			$("#containerSumber").html("");
			$("#containerPekerjaan").html("");
			$("#contentOnProgress").html('');
			count = data.itemData.length;
			$.each(data.itemData,function(i,item){
				if(item.jenis_pekerjaan==0){
					if (item.upload_file == null){
						$("#sumberNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#containerSumber").append($("#sumberNoItem").html());
					} else {
						dataCount++;
						$("#sumberWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#sumberWithItem").find("#downloadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#sumberWithItem").find("#downloadButton").attr("data-kerja",data.id_kerja);
						$("#sumberWithItem").find("#downloadButton").attr("data-url",item.upload_file);
						$("#containerSumber").append($("#sumberWithItem").html());						
					}
				} else {
					if (item.upload_file == null){
						$("#pekerjaanNoItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#pekerjaanNoItem").find("#uploadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#pekerjaanNoItem").find("#uploadButton").attr("data-kerja",data.id_kerja);						
						$("#containerPekerjaan").append($("#pekerjaanNoItem").html());
					} else {
						dataCount++
						$("#pekerjaanWithItem").find("#leftName").html(item.nama_detail_pekerjaan);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-pekerjaan",item.id_detail_pekerjaan);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-kerja",data.id_kerja);
						$("#pekerjaanWithItem").find("#downloadButton").attr("data-url",item.upload_file);
						$("#containerPekerjaan").append($("#pekerjaanWithItem").html());
					}
				}
			});
			itemCount = dataCount;
			$("#withData").find($("#lblStatus").html("Status : "+data.status));
			$("#withData").find($("#lblDeadline").html("Deadline : "+data.deadline));
			$("#withData").find($("#lblStartdate").html("Start Date : "+data.start_date));
			$("#contentOnProgress").html($("#withData").html());
		}
	}
	$("#containerPekerjaan").on('click','#uploadButton',function(){
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#manipulateModal").modal("show");		
	});
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					console.log(response);
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Data berhasil disimpan");
						$("#manipulateModal").modal("hide");
						getOnProgress();
					}else{
						alert("Gagal menyimpan data");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});
		
	});
	$("#containerPekerjaan").on('click','#editButton',function(){
		var id = $(this).attr('data-id');
		var id_kerja = $(this).attr('data-kerja');
		var id_pekerjaan = $(this).attr('data-pekerjaan');
		var url = $(this).attr('data-url');
		$("#input_id_kerja").val(id_kerja);
		$("#input_id_pekerjaan").val(id_pekerjaan);
		$("#idDetail").val(id);
		$("#manipulateForm").prop('action',baselink+"manageKerja/edit");
		$("#inputUrl").val(url);
		$("#manipulateModal").modal("show");		
	});	
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	$("#inputUrl").val("");
	  	$("#manipulateForm").prop('action',baselink+"manageKerja/add");
	});	
	$("#containerSumber").on('click','#downloadButton',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#containerPekerjaan").on('click','#downloadButton',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');		
	});	
	$("#changeStatus").click(function(){
		if (count != itemCount){
			alert('Tolong lengkapi semua data')
		} else {
			var id = $(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: baselink+'manageKerja/changeStatus',
				data:{"id":id,"access":1},
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response)
					if(data.status == "success"){
						$.redirect(baselink+"pekerjaanKlien", {'idPekerjanKlien': $("#id_client").val()});
						alert("Status berhasil diganti");

					} else {	
						alert("Gagal mengganti status")
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});				
		}	
	});
	$("#backAction").click(function(e){
		e.preventDefault();
		$.redirect(baselink+"pekerjaanKlien", {'idPekerjanKlien': $("#id_client").val()});
	});
	window.onhashchange = function() {
		alert("okok");
	}
});