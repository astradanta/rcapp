$(document).ready(function(){
	var baselink = $("#baselink").val();
	var idPekerjaanKlien = $("#idPekerjaanKlien").val();
	displayList();
	setValuePicker();
	listPekerjaanKlien();
	function displayList(){

		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/list',
			data:{"baselink":baselink,"id":idPekerjaanKlien},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listView").html('');
				$.each(data,function(i,item){
					parseList(item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function listPekerjaanKlien(){
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/listPekerjaanKlien',
			data:{"baselink":baselink,"id":idPekerjaanKlien},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				$("#listPekerjaan").html('');
				$.each(data,function(i,item){
					parseListPekerjaanKlien(i,item);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		}); 		
	}
	function parseListPekerjaanKlien(i,item){
		var text = '<tr>'+
					'<td>'+(parseInt(i)+1)+'</td>'+
					'<td>'+item.nama_pekerjaan+'</td>'+
					'</tr>';
		$("#listPekerjaan").append(text);
	}
	function parseList(data){
		console.log(data);
		var totalKerja = parseInt(data.total_kerja);
		var totalDikerjakan = parseInt(data.total_dikerjakan);
		var persentase = Math.round((totalDikerjakan/totalKerja)*100);
		$("#listItemTemplate").find("#itemLeftIcon").html('<i style="font-size: 40px;">'+persentase+'<sup>%</sup></i>')
		$("#listItemTemplate").find("#itemContainer").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemContainer").attr("data-pekerjaan",data.id_pekerjaan);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemLeftIcon").attr("data-pekerjaan",data.id_pekerjaan);		
		$("#listItemTemplate").find("#editItem").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#deleteItem").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").html(data.deadline);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-id",data.id_kerja);
		$("#listItemTemplate").find("#itemlistDeadline").attr("data-pekerjaan",data.id_pekerjaan);
		 $("#listItemTemplate").find("#namaPekerjaan").html(data.nama_pekerjaan);
		$("#listView").append($("#listItemTemplate").html());
	}
	function setValuePicker(){
			$.ajax({
				type: "POST",
				url: baselink+'pekerjaanKlien/listSelectPekerjaan',
				data:{"baselink":baselink,"id":idPekerjaanKlien},
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					$("#inputPekerjaan").append('<option value="0">Pilih Pekerjaan</option>');
					$.each(data,function(i,item){
						$("#inputPekerjaan").append('<option value="'+item.id_daftar_pekerjaan_client+'">'+item.nama_pekerjaan+'</option>');
					});
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	$("#manipulateForm").submit(function(e){
		e.preventDefault();
		if ($("#inputPekerjaan").val()==0){
			alert("Anda harus memilih pekerjaan");
		} else {
			$.ajax({
				type: "POST",
				url: $(this).prop("action"),
				data: new FormData( this ),
		      	processData: false,
		      	contentType: false,
				success: function(response){
					var data = jQuery.parseJSON(response);
					if (data.status == "success"){
						alert("Pekerjaan klien berhasil disimpan");
						$("#manipulateModal").modal("hide");
						displayList();
					}else{
						alert("Gagal menyimpan data pekerjaan klien");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        console.log(xhr.status);
			        console.log(xhr.responseText);
			        console.log(thrownError);
			    }
			});			
		}
		
	});	
	$('#startDate').datepicker({
	   autoclose: true
	});
	$('#deadLine').datepicker({
	   autoclose: true
	});
	$("#manipulateModal").on("hidden.bs.modal", function () {
	  	resetModal();
	});		
	function detail(id)	{
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/detail',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				parseDetail(data[0]);
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
	$("#listView").on('click','#editItem',function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		detail(id);
	});
	function parseDetail(data){
		console.log(data);
		$("#startDate").val(data.start_date);
		$("#deadLine").val(data.deadline);
		$("#inputPekerjaan option[value="+data.id_daftar_pekerjaan_client+"]").prop("selected",true);
		$("#idDetail").val(data.id_kerja);
		$("#manipulateModal").modal("show");
		$("#manipulateForm").prop('action',baselink+'pekerjaanKlien/edit');
		$("#modalTitle").html("Ubah Data Pekerjaan Klien");		
	}
	function resetModal(){
		$('input[type="text"]').val('');
		$("#inputPekerjaan").val($("#inputPekerjaan option:first").val());
		$("#manipulateForm").prop('action',baselink+'pekerjaanKlien/add');
		$("#modalTitle").html("Tambah Data Pekerjaan Klien");
	}
	$("#listView").on('click','#deleteItem',function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		$("#modal_delete").modal("show");
		$("#btn_modal").attr('data-id',id);
	});	
	$("#btn_modal").click(function(){
		var id = $(this).attr("data-id")
		$.ajax({
			type: "POST",
			url: baselink+'pekerjaanKlien/delete',
			data:{"baselink":baselink,"id":id},
			cache: false,
			success: function(response){
				var data = jQuery.parseJSON(response);
				if (data.status == "success"){
					alert("Data Berhasil dihapus");
					$("#modal_delete").modal("hide");
					displayList()
				} else {
					alert("Gagal menghapus data")
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});
	});	
	$('#listView').on('click','#itemContainer',function(e) { 
			var url = baselink+"manageKerja";
			$.redirect(url, {'idKerja': $(this).attr('data-id'),'idClient':idPekerjaanKlien,'idPekerjaan':$(this).attr('data-pekerjaan')});
	});	
	$('#listView').on('click','#itemlistDeadline',function(e) { 
			var url = baselink+"manageKerja";
			$.redirect(url, {'idKerja': $(this).attr('data-id'),'idClient':idPekerjaanKlien,'idPekerjaan':$(this).attr('data-pekerjaan')});
	});	
	$('#listView').on('click','#itemLeftIcon',function(e) { 
			var url = baselink+"manageKerja";
			$.redirect(url, {'idKerja': $(this).attr('data-id'),'idClient':idPekerjaanKlien,'idPekerjaan':$(this).attr('data-pekerjaan')});
	});		
});