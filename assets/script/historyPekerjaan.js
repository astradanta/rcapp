$(document).ready(function(){
	var baselink = $("#baselink").val();
	displayList();
	getListClient();
	var minDateFilter = "";
	var maxDateFilter = "";
	$('#fromDate').datepicker({
	   autoclose: true
	});
	$('#untilDate').datepicker({
	   autoclose: true
	});
	$("#fromDate").change(function(){
		minDateFilter = new Date(this.value).getTime();
		$("#example2").DataTable().draw();
	});
	$("#untilDate").change(function(){
		maxDateFilter = new Date(this.value).getTime();
		$("#example2").DataTable().draw();
	});	
	function displayList(){
		$.ajax({
			type: "POST",
			url: baselink+'historyPekerjaan/list',
			data:{"baselink":baselink},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				$("#listView").html("");
				$.each(data,function(i,item){
					addItemList(item,i+1);
				});
				$("#example2").DataTable();				

			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	}
    function addItemList(data,count){
    	var text = '<tr>'+
	            		'<td>'+count+'</td>'+
	             		'<td>'+data.nama_client+'</td>'+
	             		'<td>'+data.nama_pekerjaan+'</td>'+
	             		'<td>'+data.created_at+'</td>'+
	             		'<td>Selesai</td>'+
	             		'<td><button type="button" class="btn btn-primary" id="detailHistory" data-url="'+baselink+'detailHistory/'+data.id_history_kerja+'">Detail</button></td>'+
					'</tr>';
    	$("#listView").append(text);
    }
    $("#listView").on('click','#archiveDownload',function(){
		var url = $(this).attr('data-url');
		window.open(url, 'name');    	
    });	
    $("#listView").on('click','#detailHistory',function(){
		var url = $(this).attr('data-url');
		window.location.href = url;    	
    });	
    $("#temp").change(function(){
    	$("#example2").DataTable().search( $(this).val() ).draw();
    });
	function getListClient(){
			var baselink = $('#baselink').val();
			$.ajax({
				type: "POST",
				url: baselink+'listClient',
				cache: false,
				success: function(response){
					var data = jQuery.parseJSON(response);

					$.each(data,function(i,item){
						$("#selectPicker").append('<option value="'+item.nama+'">'+item.nama+'</option>');
					});								
				},
				error: function (xhr, ajaxOptions, thrownError) {
		           console.log(xhr.status);
		           console.log(xhr.responseText);
		           console.log(thrownError);
		       }
			});
	}
	$("#selectPicker").change(function(){
		var keyword = $(this).val();						
		$.ajax({
			type: "POST",
			url: baselink+'historyPekerjaan/search',
			data:{"baselink":baselink,"keyword":keyword},
			cache: false,
			success: function(response){
				console.log(response);
				var data = jQuery.parseJSON(response);
				$("#example2").DataTable().destroy();				
				$("#listView").html('');
				$.each(data,function(i,item){
					addItemList(item,i+1);
				});
				$("#example2").DataTable();
			},
			error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(xhr.responseText);
		        console.log(thrownError);
		    }
		});		
	});
$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[3]).getTime();

    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
);
});