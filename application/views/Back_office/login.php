<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <h3><b style="color: #fe0000">RED</b> Consulting</h3>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" id="loginContainer" >
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="<?php echo base_url() ?>login/signin" method="post" id="loginForm">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" id="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" id= "password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <a href="#">I forgot my password</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>

<!-- forgot container -->
  <div id="forgetContainer" style="display: none;" class="login-box-body">
    <p class="login-box-msg">Forgot password form</p>

    <form action="<?php echo base_url() ?>login/forget" method="post" id="forgetForm">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" id="emailForget">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
         <button type="button" class="btn btn-danger btn-block btn-flat " id="cancel">Cancel</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" id="Send">Send</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  </div>
  <!-- /.login-box-body -->
</div>