<div class="content-wrapper">
	 <section class="content">
	 	<div class="row">
	 		<div class="col-md-12" id="viewPanel">
	          <div class="box box-danger">
	            <div class="box-header">
	              <h3 class="box-title">Daftar Detail Pekerjaan</h3>
	              <button type="button" class="btn btn-primary" style="margin-left: 20px" data-toggle="modal" data-target="#manipulateModal">Tambah</button>	              
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	              <table id="pekerjaanTable" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th width="15%">No</th>
	                  <th width="20%">Pekerjaan</th>
	                  <th width="20%">Detail Pekerjaan</th>
	                  <th width="30%">Jenis Pekerjaan</th>
	                  <th width="70">Aksi</th>
	                </tr>
	                </thead>
	                <tbody id="listView">
 						            
	               
	                </tbody>
	              </table>
	            </div>
	            <!-- /.box-body -->
	          </div>	 			
	 		</div>
	 	</div>
	 </section>
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title" id="modalTitle">Tambah Data Detail Pekerjaan</h3>
	            </div>
	            <form action="<?php echo(base_url()) ?>detailPekerjaan/add" method="post" id="manipulateForm" enctype="multipart/form-data">
	              <div class="box-body">
	              	<input type="hidden" id="idDetail" name="idDetail" value="">
	              	<input type="hidden" name="access" value="1">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama Detail Pekerjaan</label>
	                  <input type="text" class="form-control" name="nama" id="inputName" placeholder="Ketikan Nama" required="">
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deskripsi</label>
	                  <textarea id="inputDeskripsi" name="deskripsi" class="form-control"></textarea>
	                </div>
		              <div class="form-group">
		                <label>Pekerjaan</label>
		                <select class="form-control select2" id="inputPekerjaan" name="pekerjaan" style="width: 100%;">
		                </select>
		              </div>
		              <div class="form-group">
		              	<label>Jenis Pekerjaan</label>
							<div class="radio">
			                    <label class="radio-inline"><input type="radio" id="firstRadio" name="jenisPekerjaan" value="0">Klien Upload</label>
			                    <label class="radio-inline"><input type="radio" id="secondRadio" name="jenisPekerjaan" value="1">Klien Download</label>
			                </div>	

		              </div>		              	                
		              <input type="hidden" name="idPekerjaan" id="editIdPekerjaan" value="">
		              <input type="hidden" name="access" value="1">               	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div style="display: none;" id="actionItemTemplate">
        <button type="button" class="btn btn-danger" id="deleteList" data-id=""><i class="fa fa-trash"></i></button>
        <button type="button" class="btn btn-info" style="margin-left: 3px;margin-right: 3px;" id="editList" data-id=""><i class="fa fa-pencil"></i></button>
        <button type="button" class="btn btn-warning" id="detailList"><i class="fa fa-list"></i></button>		
</div>
<div class="modal fade" id="detailModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Detail</h3>
	            </div>
	            <form action="" method="post" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama Pekerjaan</label>
	                  <input type="text" class="form-control" name="nama" id="detailName" placeholder="Ketikan Nama" required="" disabled="">
	                </div>
                  <div class="form-group">
                    <label>Nama Detail Pekerjaan </label>
                     <input type="text" class="form-control" name="nama" id="detailDetailPekerjaan" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div>                                                                         	                
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deskripsi</label>
	                  <textarea id="detailDeskripsi" name="deskripsi" class="form-control" disabled=""></textarea>
	                </div>
                  <div class="form-group">
                    <label>Dibuat Pada </label>
						<input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
						<input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Oleh </label>
						<input type="text" class="form-control" name="nama" id="detailUpdateBy" placeholder="" required="" disabled="">  
                  </div> 	                
		              <input type="hidden" name="access" value="1">
		              <div class="form-group">
		              	 <label id="editGenerate"></label>		              	
		              </div>	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div> 