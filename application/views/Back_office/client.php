  <!-- Full Width Column -->
  <input type="hidden" id="id_role" name="" value="<?php echo($_SESSION['role_id']) ?>">
  <div class="content-wrapper">
	<div class="landing-container">
        <div class="row" <?php if($_SESSION['role_id']>2) echo ('style="display:none;"') ?>>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="countClient">50</h3>

              <p>Client</p>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="countInvoice">25</h3>

              <p>Pekerjaan Tax</p>
            </div>

          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3 id="countKerja">44</h3>

              <p>Pekerjaan Finance</p>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="countConfirm">65</h3>

              <p>Semua Pekerjaan</p>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
		<div class="box box-primary" style="background-color: #f3f3f3" id="dataList">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Client List</h3>

              <div class="box-body">
                  <div >
                    <div class="row" style="">
                      <div class="form-group col-md-6">
                                  <input type="email" class="form-control" id="keyword" placeholder="Cari">
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="selectorPekerjaan">
                                  <option>Alabama</option>
                                  <option>Alaska</option>
                                  <option>California</option>
                                  <option>Delaware</option>
                                  <option>Tennessee</option>
                                  <option>Texas</option>
                                  <option>Washington</option>
                                </select>
                              </div>
                    </div>
                    <div class="row" id="viewClientContainer">

                    </div> 
                    <div class="row">
                      <div class="col-sm-5"></div>
                      <div class="col-sm-7" id="clientPagging">
                          <div class="dataTables_paginate paging_simple_numbers pull-right" id="viewClientPagging">
                              <ul class="pagination" id="paginationContainer">
                                <li class="paginate_button previous disabled" id="viewClient_previous">
                                  <a href="#" aria-controls="tableLog" data-dt-idx="0" tabindex="0">Previous</a>
                                </li>
                                <li class="paginate_button next" id="viewClient_next">
                                  <a href="#" aria-controls="tableLog" data-dt-idx="8" tabindex="0">Next</a>
                                </li>
                              </ul>
                            </div>
                      </div>
                    </div>                         
                  </div>      
              </div>

              

            </div>
          </div>
		<div class="box box-danger" id="manipulationPanel" style="display: none;">
			<div class="box-header with-border">
              <h3 class="box-title" id="formTitle">Tambah Data Client</h3>
            </div>
            <form id="manipulationForm" action="<?php echo(base_url()) ?>addClient" method="POST" enctype="multipart/form-data">
            	<div class="box-body">
            		<div class="col-xs-6">
            			<input type="hidden" name="idClient" id="idClientInput" value="">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Nama</label>
		                  <input type="text" class="form-control" name="nama" id="inputNama" placeholder="Ketikan Nama">
		                </div>
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Jenis Perusahaan</label>
		                  <input type="text" class="form-control" name="jenis" id="inputJenis" placeholder="Ketikan Jenis Perusahaan">
		                </div>
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Alamat</label>
		                  <textarea id="inputAlamat" name="alamat" class="form-control"></textarea>
		                </div>
		               	<div class="form-group">
		                  <label for="exampleInputEmail1">No Telepon</label>
		                  <input type="text" class="form-control" name="telepon" id="inputPhone" placeholder="Ketikan no telepon">
		                </div>
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Email</label>
		                  <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Ketikan email klien">
		                </div>              			
            		</div>
            		<div class="col-xs-6">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Deskripsi</label>
		                  <textarea id="inputDeskripsi" name="deskripsi" class="form-control"></textarea>
		                </div>
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Mulai Kontrak</label>
		                  <input type="text" name="startContract" class="form-control" id="startContract">
		                </div>
		               	<div class="form-group" >
		                  <label for="exampleInputEmail1">Selesai Kontrak</label>
		                  <input type="text" name="endContract" class="form-control" id="endContract">
		                </div>   
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Logo</label>
		                  <input type="file" class="form-control" name="logo" id="inputLogo">
		                </div>
		                <input type="hidden" name="btn_save" value="0">         			
            		</div>
            		<!--  -->
            	</div>
            	<div class="box-footer" style="text-align: right;">

                	<button type="button" class="btn btn-danger" id="btn_cancel">Cancel</button>            		
                	<button type="submit" class="btn btn-success" id="btn_save" >Simpan</button>
              	</div>
            </form>
		</div>
	</div>
	
  </div>
  <!-- /.content-wrapper -->
 
<!-- view -->
<div id="item" style="display: none;">
	<div class="col-xs-12 col-sm-6 col-md-3">
	          <!-- small box -->
	          <div class="small-box item-box bg-white">
	            <div class="inner item" id="idContainer" data-id="">
	              <h4 id="itemName">PT 123</h4>
	              <h3 id="itemCount">3</h3>
	              <p>Total Pekerjaan</p>
	            </div>
              <?php if($_SESSION['role_id']<3) { ?>
	            <div  class="small-box-footer">
                <a href="" data-action="item" data-id="" id="itemPekerjaan" style=""><i class="fa fa-list"></i></a>
	              <a href="" data-action="edit" data-id="" id="itemEdit" style="margin-right: 10px;margin-left:  10px;"><i class="fa fa-pencil"></i></a>&nbsp;
	              <a href="" data-action="delete" data-id="" id="itemDelete" style=""><i class="fa fa-trash"></i></a>
	            </div>
              <?php } ?>
	          </div>
	        </div> 
</div>
<div id="addPanel" style="display: none;">
				<div class="col-xs-12 col-sm-6 col-md-3">
	          <!-- small box -->
	          <div class="small-box item-box bg-white addClient" id="addClient">
	            <div class="inner">
	              	<h3 style="line-height: 110px"><i class="fa fa-plus"></i></h3>
	            </div>
	          
	            <div  class="small-box-footer">
	              &nbsp;
	            </div>
	          </div>
	 </div>
</div>
<input type="hidden" id="countPekerjaan" name="" value="0">
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div class="modal fade" id="pekerjaanModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Data Pekerjaan Client</h3>
              </div>
              <form action="<?php echo(base_url()); ?>manageClient/savePekerjaan" class="form-horizontal" method="post" id="pekerjaanForm" enctype="multipart/form-data">
                <div class="box-body" id="pekerjaanList">
                                                                 
                </div>
                <!-- /.box-body -->
                <input type="hidden" name="access" value="1">
                <input type="hidden" id="idDetail" name="idDetail">
                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
              <div class="overlay" style="display: none;" id="loadingPekerjaan">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<select style="display: none;" id="taxUser">
  <?php 
    $text = "<option value='0'>Pilih PIC</option>";
    foreach ($tax_user as $key) {
      $text .="<option value='".$key->id_user."'>".$key->nama."</option>";
    }
    echo($text);
   ?>
</select>
<select style="display: none;" id="financeUser">
  <?php 
    $text = "<option value='0'>Pilih PIC</option>";
    foreach ($finance_user as $key) {
      $text .="<option value='".$key->id_user."'>".$key->nama."</option>";
    }
    echo($text);
   ?>
</select>
<div id="itemPekerjaanTemplate" style="display: none;">
                    <div class="row">
                      <input type="hidden" id="status" name="status">
                      <div class="col-md-8">
                      <label class="checkbox-inline" id="checkPekerjaan"><input type="checkbox" value="">Option 1</label>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1" class="col-md-2">PIC</label>
                          <div class="col-md-10">
                            <select id="picSelect" name="pic" class="form-control col-md-10">
                              
                            </select>                             
                          </div>

                        </div>                        
                      </div>
                    </div>   
</div>