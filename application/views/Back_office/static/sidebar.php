  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $_SESSION['photo'];?>" class="img-circle" style="height: 45px;" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['nama']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if($this->uri->segment(1)=="home") echo("active") ?> treeview">
          <a href="<?php echo(base_url()) ?>home">
            <i class="fa fa-dashboard"></i> <span>Home</span>
          </a>
          
        </li>
        <?php if($_SESSION['role_id'] < 3){ ?>
        <li class="<?php if($this->uri->segment(1)=="user"||$this->uri->segment(1)=="pekerjaan"||$this->uri->segment(1)=="detailPekerjaan") echo("active") ?> treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if($_SESSION['role_id'] == 2){ ?>
            <li><a href="<?php echo(base_url()) ?>user"><i class="fa fa-circle-o"></i> User</a></li>
            <?php } ?>
            <li><a href="<?php echo(base_url()) ?>pekerjaan"><i class="fa fa-circle-o"></i> Pekerjan</a></li>
            <li><a href="<?php echo(base_url()) ?>detailPekerjaan"><i class="fa fa-circle-o"></i> Detail Pekerjaan</a></li>
           
          </ul>
        </li>
        <?php } ?>
        <?php if($_SESSION['role_id'] < 3){ ?>
        <li class="<?php if($this->uri->segment(1)=="invoice"||$this->uri->segment(1)=="confirm"||$this->uri->segment(1)=="receipt") echo("active") ?> treeview">
          <a href="#">
            <i class="ion ion-cash"></i>
            <span>Payment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>invoice"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="<?php echo base_url(); ?>confirm"><i class="fa fa-circle-o"></i> Konfirmasi Pembayaran</a></li>
            <li><a href="<?php echo base_url(); ?>receipt"><i class="fa fa-circle-o"></i> Receipt</a></li>
            
          </ul>
        </li>
        <?php } ?>        
        
        
        <li><a href="#"><i class="fa fa-book"></i> <span>Company Profile</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="saran") echo("active") ?> treeview"><a href="<?php echo base_url(); ?>saran"><i class="fa fa-commenting"></i> <span>Saran Masukan</span></a></li>
        <li class="<?php if($this->uri->segment(1)=="historyPekerjaan") echo("active") ?> treeview"><a href="<?php echo base_url(); ?>historyPekerjaan"><i class="fa fa-history"></i> <span>History Pekerjaan</span></a></li>

        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>