 <footer class="main-footer" style="background-color: #757575;color: #ffffff">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.redirect.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- DataTables -->
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/script/global.js"></script>
<?php if ($this->uri->segment(1) == 'home') { ?>
<script src="<?php echo base_url()?>assets/script/home.js"></script>
<?php }?>
<?php if ($this->uri->segment(1) == '') { ?>
<script src="<?php echo base_url()?>assets/script/home.js"></script>
<?php }?>
<?php if($this->uri->segment(1) == 'log'){?>
  <script src="<?php echo base_url()?>assets/script/log.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'user'){?>
  <script src="<?php echo base_url()?>assets/script/user.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'pekerjaan'){?>
  <script src="<?php echo base_url()?>assets/script/pekerjaan.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'detailPekerjaan'){?>
  <script src="<?php echo base_url()?>assets/script/detailPekerjaan.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'pekerjaanKlien'){?>
  <script src="<?php echo base_url()?>assets/script/pekerjaanKlien.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'historyPekerjaan'){?>
  <script src="<?php echo base_url()?>assets/script/historyPekerjaan.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'invoice'){?>
  <script src="<?php echo base_url()?>assets/script/invoice.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'confirm'){?>
  <script src="<?php echo base_url()?>assets/script/confirm.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'receipt'){?>
  <script src="<?php echo base_url()?>assets/script/receipt.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'saran'){?>
  <script src="<?php echo base_url()?>assets/script/saran.js"></script>
<?php } ?>
<?php if($this->uri->segment(1) == 'manageKerja'){?>
  <script src="<?php echo base_url()?>assets/script/manageKerja.js"></script>
<?php } ?>
</body>
</html>