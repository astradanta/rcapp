
<!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url()?>assets/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
	$("#loginForm").submit(function(e){
		e.preventDefault();
		var baselink = $("#loginForm").prop('action');
		var email = $('#email').val();
		var password = $('#password').val();
		var globalBaselink = $('#baselink').val();
		$.ajax({
			type: "POST",
			url: baselink,
			data: {'email': email,'password':password},
			cache: false,
			success: function(response){
			    console.log(response);
				data = jQuery.parseJSON(response);
				alert(data.message);
				if (data.status == 1){
					window.location.replace(globalBaselink)
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
	           console.log(xhr.status);
	           console.log(xhr.responseText);
	           console.log(thrownError);
	       }
		});
	});

	$('#cancel').click(function(){
		console.log('camcel');
		$('#forgetContainer').slideUp();
		$('#loginContainer').slideDown();
	});
	$('a').click(function(){
		$('#loginContainer').slideUp();
		$('#forgetContainer').slideDown();
		return false;
	})

	$("#forgetForm").submit(function(e){
		e.preventDefault();
		var baselink = $("#forgetForm").prop('action');
		var email = $('#emailForget').val();
		$.ajax({
			type: "POST",
			url: baselink,
			data: {'email': email},
			cache: false,
			success: function(response){
				data = jQuery.parseJSON(response);
				if (data.status == 1){
				$('#forgetContainer').slideUp();
				$('#loginContainer').slideDown();
				}
				alert(data.message);
			},
			error: function (xhr, ajaxOptions, thrownError) {
	           console.log(xhr.status);
	           console.log(xhr.responseText);
	           console.log(thrownError);
	       }
		});
	});
</script>
</body>
</html>