<?php 
  if($_SESSION['role_id']<3){
    $client = 'class="col-lg-3 col-xs-6"';
    $invoice = 'class="col-lg-3 col-xs-6"';
    $pekerjaan = 'class="col-lg-3 col-xs-6"';
    $konfirmasi = 'class="col-lg-3 col-xs-6"';
  } else{
    $client = 'class="col-lg-6 col-xs-6"';
    $invoice = 'class="col-lg-3 col-xs-6" style="display:none;"';
    $pekerjaan = 'class="col-lg-6 col-xs-6"';
    $konfirmasi = 'class="col-lg-3 col-xs-6" style="display:none;"';
  }

?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div <?php echo($client) ?>>
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="countClient">50</h3>

              <p>Total Client</p>
            </div>
            <div class="icon">
              <i class="ion ion-briefcase"></i>
            </div>
            <a href="<?php echo(base_url()) ?>manageClient" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div <?php echo($invoice) ?>>
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="countInvoice">25</h3>

              <p>Invoice</p>
            </div>
            <div class="icon">
              <i class="ion ion-calculator"></i>
            </div>
            <a href="<?php echo(base_url()) ?>invoice" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div <?php echo($pekerjaan) ?>>
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="countKerja">44</h3>

              <p>Daftar Pekerjaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
            <a href="<?php echo(base_url()) ?>pekerjaan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div <?php echo($konfirmasi) ?>>
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="countConfirm">65</h3>

              <p>Konfirmasi Pembayaran</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a href="<?php echo(base_url()) ?>confirm" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box box-primary" style="background-color: #f3f3f3">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Client List</h3>

              <div class="box-body">
                  <div id="dataList">
                    <div class="row" style="">
                      <div class="form-group col-md-6">
                                  <input type="email" class="form-control" id="keyword" placeholder="Cari">
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="selectorPekerjaan">
                                  <option>Alabama</option>
                                  <option>Alaska</option>
                                  <option>California</option>
                                  <option>Delaware</option>
                                  <option>Tennessee</option>
                                  <option>Texas</option>
                                  <option>Washington</option>
                                </select>
                              </div>
                    </div>
                    <div class="row" id="viewClientContainer">

                    </div> 
                    <div class="row">
                      <div class="col-sm-5"></div>
                      <div class="col-sm-7" id="clientPagging">
                          <div class="dataTables_paginate paging_simple_numbers pull-right" id="viewClientPagging">
                              <ul class="pagination" id="paginationContainer">
                                <li class="paginate_button previous disabled" id="viewClient_previous">
                                  <a href="#" aria-controls="tableLog" data-dt-idx="0" tabindex="0">Previous</a>
                                </li>
                                <li class="paginate_button next" id="viewClient_next">
                                  <a href="#" aria-controls="tableLog" data-dt-idx="8" tabindex="0">Next</a>
                                </li>
                              </ul>
                            </div>
                      </div>
                    </div>                         
                  </div>
                        <div class="box box-danger" id="manipulationPanel" style="display: none;">
      <div class="box-header with-border">
              <h3 class="box-title" id="formTitle">Tambah Data Client</h3>
            </div>
            <form id="manipulationForm" action="<?php echo(base_url()) ?>addClient" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="col-xs-6">
                  <input type="hidden" name="idClient" id="idClientInput" value="">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input type="text" class="form-control" name="nama" id="inputNama" placeholder="Ketikan Nama">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Perusahaan</label>
                      <input type="text" class="form-control" name="jenis" id="inputJenis" placeholder="Ketikan Jenis Perusahaan">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Alamat</label>
                      <textarea id="inputAlamat" name="alamat" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">No Telepon</label>
                      <input type="text" class="form-control" name="telepon" id="inputPhone" placeholder="Ketikan no telepon">
                    </div>
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Ketikan email klien">
                    </div>                    
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Deskripsi</label>
                      <textarea id="inputDeskripsi" name="deskripsi" class="form-control"></textarea>
                    </div>
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Mulai Kontrak</label>
                      <input type="text" name="startContract" class="form-control" id="startContract">
                    </div>
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Selesai Kontrak</label>
                      <input type="text" name="endContract" class="form-control" id="endContract">
                    </div>   
                    <div class="form-group" >
                      <label for="exampleInputEmail1">Logo</label>
                      <input type="file" class="form-control" name="logo" id="inputLogo">
                    </div>
                    <input type="hidden" name="btn_save" value="0">               
                </div>
                <div class="col-xs-12">
                  <label style="margin-top: 20px;">Daftar Pekerjaan</label>
                  <div class="col-xs-12" id="checkContainer">

                  </div>
                </div>
                <!--  -->
              </div>
              <div class="box-footer" style="text-align: right;">

                  <button type="button" class="btn btn-danger" id="btn_cancel">Cancel</button>                
                  <button type="submit" class="btn btn-success" id="btn_save" >Simpan</button>
                </div>
            </form>
    </div>          
              </div>

              

            </div>
          </div>
          <!-- /.nav-tabs-custom -->

          <!-- Chat box -->
          
          <!-- /.box (chat box) -->

          <!-- TO DO List -->

          <!-- /.box -->

          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
          
          </div>
          <!-- /.box -->        

          
            <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Recent Log</h3>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <table id="tableLog" class="table table-bordered table-striped">
                <thead style="display: none;">
                <tr>
                  <th>Activity</th>
                  <th>date</th>
                </tr>
                </thead>
                <tbody id="listView">
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            
          </div>
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

    <div id="item" style="display: none;">
  <div class="col-md-4 col-sm-6 col-xs-12">
            <!-- small box -->
            <div class="small-box item-box bg-white">
              <div class="inner item" id="idContainer" data-id="">
                <h4 id="itemName">PT 123</h4>
                <h3 id="itemCount">3</h3>
                <p>Total Pekerjaan</p>
              </div>
            </div>
          </div> 
</div>
<div id="addPanel" style="display: none;">
        <div class="col-md-6">
            <!-- small box -->
            <div class="small-box item-box bg-white addClient" id="addClient">
              <div class="inner">
                  <h3 style="line-height: 110px"><i class="fa fa-plus"></i></h3>
              </div>
            
              <div  class="small-box-footer">
                &nbsp;
              </div>
            </div>
   </div>
</div>
<input type="hidden" id="countPekerjaan" name="" value="0">
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->