<div class="content-wrapper">
	 <section class="content">
	 	<div class="row">
	 		<div class="col-md-12" id="addPanel" style="display: none;">
	          <div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Form Tambah Pekerjaan</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            <form action="" method="post" id="addForm" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama Pekerjaan</label>
	                  <input type="text" class="form-control" id="addName" placeholder="Ketikan Nama" name="nama" required="" autocomplete="off">
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deskripsi</label>
	                  <textarea id="addDeskripsi" name="deskripsi" class="form-control"></textarea>
	                  <input type="hidden" name="access" value="1">
	                </div>	
					<div class="form-group">
                    <label>Kategori Pekerjaan </label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="kategori1" name="kategori" value="Project" checked="">Project</label>
                          <label class="radio-inline"><input type="radio" id="kategori2" name="kategori" value="Reguler">Reguler</label>
                      </div>  

                  </div> 
                  <div class="form-group">
                    <label>Type Pekerjaan </label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="type1" name="tipe" value="Tax" checked="">Tax</label>
                          <label class="radio-inline"><input type="radio" id="type2" name="tipe" value="Finance">Finance</label>
                      </div>  

                  </div>

	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer"> 
	              	<button type="button" class="btn btn-danger" id="btn_close"><i class="fa fa-close">&nbsp;Close</i></button>  
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>	 			
	 		</div>
	 		<div class="col-md-12" id="viewPanel">
	          <div class="box box-danger">
	            <div class="box-header">
	              <h3 class="box-title">Daftar Pekerjaan</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	              <div class="" style="padding-bottom: 10px"> 
	              	<button type="button" class="btn btn-primary " id="btn_add"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
	              </div>	
	              <table id="pekerjaanTable" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th width="20%">No</th>
	                  <th width="25%">Nama</th>
	                  <th width="20%">Deskripsi</th>
	                  <th width="10%">Kategori</th>
	                  <th width="10%">Tipe</th>
	                  <th width="70">Aksi</th>
	                </tr>
	                </thead>
	                <tbody id="listView">
 						            
	               
	                </tbody>
	              </table>
	            </div>
	            <!-- /.box-body -->
	          </div>	 			
	 		</div>
	 	</div>
	 </section>
</div>
<div class="modal fade" id="editModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Form Ubah Pekerjaan</h3>
	            </div>
	            <form action="" method="post" id="editForm" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama Pekerjaan</label>
	                  <input type="text" class="form-control" name="nama" id="editName" placeholder="Ketikan Nama" required="">
	                </div>
                  <div class="form-group">
                    <label>Kategori Pekerjaan </label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="kategori1Edit" name="kategori" value="Project">Project</label>
                          <label class="radio-inline"><input type="radio" id="kategori2Edit" name="kategori" value="Reguler">Reguler</label>
                      </div>  

                  </div> 
                  <div class="form-group">
                    <label>Type Pekerjaan </label>
                       <div class="radio">
                          <label class="radio-inline"><input type="radio" id="tipe1Edit" name="tipe" value="Tax" checked="">Tax</label>
                          <label class="radio-inline"><input type="radio" id="tipe2Edit" name="tipe" value="Finance">Finance</label>
                      </div>  

                  </div>                   	                
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deskripsi</label>
	                  <textarea id="editDeskripsi" name="deskripsi" class="form-control"></textarea>
	                </div>

		              <input type="hidden" name="idPekerjaan" id="editIdPekerjaan" value="">
		              <input type="hidden" name="access" value="1">
		              <div class="form-group">
		              	 <label id="editGenerate"></label>		              	
		              </div>	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
 <div class="modal fade" id="detailModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Detail</h3>
	            </div>
	            <form action="" method="post" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama Pekerjaan</label>
	                  <input type="text" class="form-control" name="nama" id="detailName" placeholder="Ketikan Nama" required="" disabled="">
	                </div>
                  <div class="form-group">
                    <label>Kategori Pekerjaan </label>
                     <input type="text" class="form-control" name="nama" id="detailKategori" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div> 
                  <div class="form-group">
                    <label>Type Pekerjaan </label>
						<input type="text" class="form-control" name="nama" id="detailTipe" placeholder="Ketikan Nama" required="" disabled="">  
                  </div>                                                                         	                
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Deskripsi</label>
	                  <textarea id="detailDeskripsi" name="deskripsi" class="form-control" disabled=""></textarea>
	                </div>
                  <div class="form-group">
                    <label>Dibuat Pada </label>
						<input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
						<input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Oleh </label>
						<input type="text" class="form-control" name="nama" id="detailUpdateBy" placeholder="" required="" disabled="">  
                  </div> 	                
		              <input type="hidden" name="access" value="1">
		              <div class="form-group">
		              	 <label id="editGenerate"></label>		              	
		              </div>	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>       
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div style="display: none;" id="actionItemTemplate">
        <button type="button" class="btn btn-danger" id="deleteList" data-id=""><i class="fa fa-trash"></i></button>
        <button type="button" class="btn btn-info" style="margin-left: 3px;margin-right: 3px;" id="editList" data-id=""><i class="fa fa-pencil"></i></button>
        <button type="button" class="btn btn-warning" id="detailList"><i class="fa fa-list"></i></button>
</div>
<select style="display: none;" id="taxUser">
	<?php 
		$text = "<option value='0'>Pilih PIC</option>";
		foreach ($tax_user as $key) {
			$text .="<option value='".$key->id_user."'>".$key->nama."</option>";
		}
		echo($text);
	 ?>
</select>
<select style="display: none;" id="financeUser">
	<?php 
		$text = "<option value='0'>Pilih PIC</option>";
		foreach ($finance_user as $key) {
			$text .="<option value='".$key->id_user."'>".$key->nama."</option>";
		}
		echo($text);
	 ?>
</select>