<div class="content-wrapper">
	 <section class="content">
	 	<div class="row">
	 		<div class="col-md-12" id="addPanel" style="display: none;">
	          <div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Form Tambah User</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            <form action="" class="form-horizontal" method="post" id="addForm" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Nama User</label>
	                  <div class="col-md-9">
	                  	<input type="text" class="form-control col-md-10" id="addName" placeholder="Ketikan Nama" name="nama" required="" autocomplete="off">
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1" class="col-sm-2 control-label">Email User</label>
	                  <div class="col-md-9">
	                  	<input type="email" class="form-control" name="email" id="addEmail" placeholder="Ketikan Email" required="" autocomplete="off">
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputFile" class="col-sm-2 control-label">Password User</label>
	                  <div class="col-md-9">
	                  	<input type="password" class="form-control" id="addPassword" placeholder="Password" name="password" required="">
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputFile" class="col-sm-2 control-label">Ulangi Password</label>
	                  <div class="col-md-9">
	                  	<input type="password" class="form-control" id="addRetype" placeholder="Ulangi password" required="">
	                  </div>
	                  <input type="hidden" name="access" value="1">
	                </div>
		              <div class="form-group">
		                <label class="col-sm-2 control-label">Jenis User</label>
		                <div class="col-md-9">
			                <select class="form-control select2" id="addJenis" name="addJenis" style="width: 100%;">
		                	<?php foreach ($role as $key) { ?>
		                 		 <option value="<?php echo($key->id_role) ?>"><?php echo($key->role_name) ?></option>		                		
		                	<?php } ?>
			                  
			                </select>		                	
		                </div>
		              </div>
		                <div class="form-group" >
		                  <label class="col-sm-2 control-label" for="exampleInputEmail1">Photo</label>
		                  <div class="col-md-9">
		                  	<input type="file" class="form-control" name="logo" id="inputLogo">
		                  </div>
		                </div>		              
		              <div class="form-group">
		              	 <label id="addGenerate" class="col-sm-2 control-label"></label>		              	
		              </div>		              	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	              	<button type="button" class="btn btn-danger" id="btn_close"><i class="fa fa-close">&nbsp;Close</i></button>
	              	<button type="button" class="btn btn-default btnGenerate" data-target="addGenerate">Generate Password</button>     
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>           
	          </div>	 			
	 		</div>
	 		<div class="col-md-12" id="viewPanel">
	          <div class="box box-danger">
	            <div class="box-header">
	              <h3 class="box-title">Daftar User</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	              <div class="" style="padding-bottom: 10px"> 
	              	<button type="button" class="btn btn-primary " id="btn_add"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
	              </div>
	           
	              <table id="userTable" class="table table-bordered table-striped">
	                <thead>
	                <tr>
	                  <th width="5%">No</th>
	                  <th width="25%">Nama</th>
	                  <th width="25%">Email</th>
	                  <th width="30%">Jenis User</th>
	                  <th width="100">Aksi</th>
	                </tr>
	                </thead>
	                <tbody id="listView">
 						            
	               
	                </tbody>
	              </table>
	            </div>
	            <!-- /.box-body -->
	          </div>	 			
	 		</div>
	 	</div>
	 </section>
</div>
<div class="modal fade" id="editModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Form Ubah User</h3>
	            </div>
	            <form action="" method="post" id="editForm" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama User</label>
	                  <input type="text" class="form-control" name="nama" id="editName" placeholder="Ketikan Nama" required="">
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputPassword1">Email User</label>
	                  <input type="email" class="form-control" name="email" id="editEmail" placeholder="Ketikan Email" required="">
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputFile">Password User</label>
	                  <input type="password" class="form-control" id="editPassword" placeholder="Password" name="password" required="">
	                </div>
	                <div class="form-group">
	                  <label for="exampleInputFile">Ulangi Password</label>
	                  <input type="password" class="form-control" id="editRetype" placeholder="Ulangi password" required="">
	                </div>
		              <div class="form-group">
		                <label>User Role</label>
		                <select class="form-control select2" id="editJenis" name="editJenis" style="width: 100%;">
		                	<?php foreach ($role as $key) { ?>
		                 		 <option value="<?php echo($key->id_role) ?>"><?php echo($key->role_name) ?></option>		                		
		                	<?php } ?>
		                	<option value="0">Klien</option>
		                </select>
		              </div>
		              <div class="form-group" >
		                  <label for="exampleInputEmail1">Photo</label>
		                  <div >
		                  	<input type="file" class="form-control" name="logo" id="editLogo">
		                  </div>
		                </div>	
		              <input type="hidden" name="idUser" id="editIdUser" value="">
		              <input type="hidden" name="access" value="1">
		              <div class="form-group">
		              	 <label id="editGenerate"></label>		              	
		              </div>	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	                <button type="button" class="btn btn-default btnGenerate" data-target="editGenerate">Generate Password</button>
	                <button type="submit" class="btn btn-success pull-right">Simpan</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div style="display: none;" id="actionItemTemplate">
        <button type="button" class="btn btn-danger" id="deleteList" data-id=""><i class="fa fa-trash"></i></button>
        <button type="button" class="btn btn-info" style="margin-left: 3px;margin-right: 3px;" id="editList" data-id=""><i class="fa fa-pencil"></i></button>
        <button type="button" class="btn btn-warning" id="detailList"><i class="fa fa-list"></i></button>	
</div>
 <div class="modal fade" id="detailModal">
          <div class="modal-dialog">
			<div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Detail</h3>
	            </div>
	            <form action="" method="post" enctype="multipart/form-data">
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="exampleInputEmail1">Nama </label>
	                  <input type="text" class="form-control" name="nama" id="detailName" placeholder="Ketikan Nama" required="" disabled="">
	                </div>
                  <div class="form-group">
                    <label>Email</label>
                     <input type="text" class="form-control" name="nama" id="detailEmail" placeholder="Ketikan Nama" required="" disabled=""> 
                  </div> 
                  <div class="form-group">
                    <label>Password</label>
						<input type="password" class="form-control" name="nama" id="detailPassword" placeholder="Ketikan Nama" required="" disabled="">  
                  </div>                                                                         	                
                  <div class="form-group">
                    <label>Jenis User</label>
						<input type="text" class="form-control" name="nama" id="detailJenis" placeholder="Ketikan Nama" required="" disabled="">  
                  </div>  
                  <div class="form-group">
                    <label>Dibuat Pada </label>
						<input type="text" class="form-control" name="nama" id="detailCreateAt" placeholder="" required="" disabled="">  
                  </div> 
                  <div class="form-group">
                    <label>Diubah Pada </label>
						<input type="text" class="form-control" name="nama" id="detailUpdateAt" placeholder="" required="" disabled="">  
                  </div> 	                
		              <input type="hidden" name="access" value="1">
		              <div class="form-group">
		              	 <label id="editGenerate"></label>		              	
		              </div>	                	                
	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
	              </div>
	            </form>
	          </div>          		
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>  