  <!-- Full Width Column -->
  <div class="content-wrapper">
	<div class="landing-container">
		<div id="dataList">
			<div class="row" style="">
				<div class="form-group col-xs-3">
	                  <input type="email" class="form-control" id="keyword" placeholder="Cari">
	            </div>
	            <div class="form-group col-xs-3">
	                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="selectorPekerjaan">
	                  <option>Alabama</option>
	                  <option>Alaska</option>
	                  <option>California</option>
	                  <option>Delaware</option>
	                  <option>Tennessee</option>
	                  <option>Texas</option>
	                  <option>Washington</option>
	                </select>
	              </div>
			</div>
			<div class="row" id="viewClientContainer">

			</div>			
		</div>
		<div class="box box-danger" id="manipulationPanel" style="display: none;">
			<div class="box-header with-border">
              <h3 class="box-title" id="formTitle">Tambah Data Client</h3>
            </div>
            <form id="manipulationForm" action="<?php echo(base_url()) ?>addClient" method="POST" enctype="multipart/form-data">
            	<div class="box-body">
            		<div class="col-xs-6">
            			<input type="hidden" name="idClient" id="idClientInput" value="">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Nama</label>
		                  <input type="text" class="form-control" name="nama" id="inputNama" placeholder="Ketikan Nama">
		                </div>
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Jenis Perusahaan</label>
		                  <input type="text" class="form-control" name="jenis" id="inputJenis" placeholder="Ketikan Jenis Perusahaan">
		                </div>
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Alamat</label>
		                  <textarea id="inputAlamat" name="alamat" class="form-control"></textarea>
		                </div>
		               	<div class="form-group">
		                  <label for="exampleInputEmail1">No Telepon</label>
		                  <input type="text" class="form-control" name="telepon" id="inputPhone" placeholder="Ketikan no telepon">
		                </div>
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Email</label>
		                  <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Ketikan email klien">
		                </div>              			
            		</div>
            		<div class="col-xs-6">
		                <div class="form-group">
		                  <label for="exampleInputEmail1">Deskripsi</label>
		                  <textarea id="inputDeskripsi" name="deskripsi" class="form-control"></textarea>
		                </div>
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Mulai Kontrak</label>
		                  <input type="text" name="startContract" class="form-control" id="startContract">
		                </div>
		               	<div class="form-group" >
		                  <label for="exampleInputEmail1">Selesai Kontrak</label>
		                  <input type="text" name="endContract" class="form-control" id="endContract">
		                </div>   
		                <div class="form-group" >
		                  <label for="exampleInputEmail1">Logo</label>
		                  <input type="file" class="form-control" name="logo" id="inputLogo">
		                </div>
		                <input type="hidden" name="btn_save" value="0">         			
            		</div>
            		<div class="col-xs-12">
            			<label style="margin-top: 20px;">Daftar Pekerjaan</label>
            			<div class="col-xs-12" id="checkContainer">

            			</div>
            		</div>
            		<!--  -->
            	</div>
            	<div class="box-footer" style="text-align: right;">

                	<button type="button" class="btn btn-danger" id="btn_cancel">Cancel</button>            		
                	<button type="submit" class="btn btn-success" id="btn_save" >Simpan</button>
              	</div>
            </form>
		</div>
	</div>
	
  </div>
  <!-- /.content-wrapper -->
 
<!-- view -->
<div id="item" style="display: none;">
	<div class="col-xs-6 col-sm-4 col-md-2">
	          <!-- small box -->
	          <div class="small-box item-box bg-white">
	            <div class="inner item" id="idContainer" data-id="">
	              <h4 id="itemName">PT 123</h4>
	              <h3 id="itemCount">3</h3>
	              <p>Total Pekerjaan</p>
	            </div>
	            <div  class="small-box-footer">
	              <a href="" data-action="edit" data-id="" id="itemEdit" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>&nbsp;
	              <a href="" data-action="delete" data-id="" id="itemDelete" style="margin-left:  10px;"><i class="fa fa-trash"></i></a>
	            </div>
	          </div>
	        </div> 
</div>
<div id="addPanel" style="display: none;">
				<div class="col-xs-6 col-sm-4 col-md-2">
	          <!-- small box -->
	          <div class="small-box item-box bg-white addClient" id="addClient">
	            <div class="inner">
	              	<h3 style="line-height: 110px"><i class="fa fa-plus"></i></h3>
	            </div>
	          
	            <div  class="small-box-footer">
	              &nbsp;
	            </div>
	          </div>
	 </div>
</div>
<input type="hidden" id="countPekerjaan" name="" value="0">
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
