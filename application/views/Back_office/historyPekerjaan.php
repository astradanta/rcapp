  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">History Pekerjaan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-12" style="padding-right: 0px;">
                  <div class="form-group">
                      <div class="col-md-3">
                          <label>From Date</label>
                          <input type="text" name="" id="fromDate" class="form-control" autocomplete="off">
                       </div>
                      <div class="col-md-3">
                          <label>Until Date</label>
                          <input type="text" name="" id="untilDate" class="form-control" autocomplete="off"> 
                       </div>                       
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Klien</th>
                  <th>Pekerjaan</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="listView">
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>