	 <input type="hidden" id="idPekerjaan" name="" value="<?php echo($_SESSION['id_client']) ?>">
	 <div class="content-wrapper" style="background-color: #ecf0f5">
	<div class="landing-container">
		<div class="row">
			<div class="col-xs-12">
<!-- 				<div class="col-md-4">
					<div class="small-box bg-gray">
			            <div class="inner medPanel">
			            	<div class="col-xs-4">
			            		<img src="<?php echo($_SESSION['photoClient']) ?>" class="img-circle" style="height: 80px;width: 80px;">
			            	</div>
			            	<div class="col-xs-8" style="text-align: left;">
				              	<h4><b>Selamat Datang</b></h4>
				              	<h4>PT ABC</h4>
			            	</div>		              	

			            </div>
			         </div>
				</div> -->
<div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $_SESSION['namaClient']?></h3>
              <h5 class="widget-user-desc">Selamat Datang</h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo($_SESSION['photoClient']) ?>" alt="User Avatar" style="height: 80px;width: 80px;">
            </div>
            <div class="box-footer">
              <div class="row">
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
        </div>				
				<div class="col-md-8">
   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box2" id="invoicePanel">
            <span class="info-box-icon2 bg-yellow"><i class="ion ion-calculator"></i></span>

            <div class="info-box-content2">
              <span class="info-box-text"><b>Invoice</b></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box2" id="confirmPanel">
            <span class="info-box-icon2 bg-green"><i class="ion ion-cash"></i></span>

            <div class="info-box-content2">
              <span class="info-box-text" style="margin-top: 43px;height: 50px;line-height: 23px;white-space:  normal;"><b>Konfirmasi Pembayaran</b></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box2" id="saranPanel">
            <span class="info-box-icon2 bg-blue"><i class="fa fa-commenting"></i></span>

            <div class="info-box-content2">
              <span class="info-box-text"><b>Saran Masukan</b></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box2">
            <span class="info-box-icon2 bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content2">
              <span class="info-box-text"><b>About RC</b></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div> 								
				</div>				
			</div>
		</div>
		<div class="row col-xs-12">
			<div class="contentHeader col-xs-12">
				<h4>List Pekerjaan</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12" id="viewList" style="padding-top: 20px;">
								
			</div>			
		</div>
	</div>
  </div>
  <!-- /.content-wrapper -->

<!--  -->
<div style="display: none;" id="listItemTemplate">
		        <div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="info-box bg-aqua infoBox" style="cursor: pointer;" >
		            <span class="info-box-icon infoBoxIcon" style="" id="itemLeftIcon" data-id=""><i style="font-size: 40px;">100<sup>%</sup></i></span>

		            <div class="info-box-content ">
		              <div style="display: flex; height: 44px;" id="itemContainer" data-id="">
			              <span style="white-space: normal;" class="info-box-text col-xs-11" id="namaPekerjaan">SPT Masa (bulanan)</span>
			              <span class="info-box-number col-xs-1"><i style="line-height: 44px" class="pull-right fa fa-fw fa-arrow-right"></i></span>
		              </div>
		              <div class="progress col-xs-12">
		                <div class="progress-bar" style="width: 70%"></div>
		              </div>
		              <div class="col-xs-11" style="display: flex">
		              	<span class="info-box-text col-xs-11" id="itemlistDeadline">2018-06-17</span>
		              </div>
		              
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div> 	
</div>