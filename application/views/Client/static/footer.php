  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Ganeshcom Studio</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.redirect.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->

<!-- DataTables -->
<script src="<?php echo base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/script/client/global.js"></script>
<?php if ($this->uri->segment(2)=='home') {?>
<script src="<?php echo base_url()?>assets/script/client/home.js"></script>
<?php } elseif($this->uri->segment(2)=='log') { ?>
<script src="<?php echo base_url()?>assets/script/client/log.js"></script>
<?php } elseif($this->uri->segment(2)=='pekerjaan') { ?>
<script src="<?php echo base_url()?>assets/script/client/pekerjaan.js"></script>
<?php }elseif($this->uri->segment(2)=='invoice') { ?>
<script src="<?php echo base_url()?>assets/script/client/invoice.js"></script>
<?php }elseif($this->uri->segment(2)=='confirm') { ?>
<script src="<?php echo base_url()?>assets/script/client/confirm.js"></script>
<?php }elseif($this->uri->segment(2)=='saran') { ?>
<script src="<?php echo base_url()?>assets/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url()?>assets/script/client/saran.js"></script>
<?php }else { ?>
<script src="<?php echo base_url()?>assets/script/client/home.js"></script>
<?php } ?>

<!--  -->
</body>
</html>