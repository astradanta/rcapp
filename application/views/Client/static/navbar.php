<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">
  <input type="hidden" name="" id="baselink" value="<?php echo(base_url()); ?>client/">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="navbar-header">
          <a href="<?php echo base_url()?>client" class="navbar-brand"><b>RED</b> Consulting</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications Menu -->
            <li><a href="<?php echo(base_url())?>client/log">Log Activity</a></li>
            <li class="dropdown notifications-menu">
              <!-- Menu toggle button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- Inner Menu: contains the notifications -->
                  <ul class="menu">
                    <li><!-- start notification -->
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <!-- end notification -->
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="<?php echo ($_SESSION['photoClient']) ?>" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?php echo $_SESSION['namaClient']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="<?php echo ($_SESSION['photoClient']) ?>" class="img-circle" alt="User Image">

                  <p>
                    <?php echo $_SESSION['namaClient']; ?>
                    <small><?php
                      if ($_SESSION['statusClient'] == 1){
                        echo "Admin";
                      } else {
                        echo "Super Admin";
                      }
                    ?></small>
                  </p>
                </li>
                
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-right">
                    <button  class="btn btn-default btn-flat" id="signout">Sign out</button>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="clearfix"></div>
        <div class="navMenu" style="background-color: #bebebe">
          <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul class="nav navbar-nav" style="height: 50px;">

              </ul>
              </form>
            </div>
          </div>
          <!-- /.container-fluid -->          
        </div>

    </nav>
  </header>