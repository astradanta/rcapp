<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Saran dan Masukan
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Saran dan Masukan</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-6" style="padding-top: 20px">
          <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Tambahkan Saran/Masukan</h3>
                </div>
                <!-- /.box-header -->
              <form action="<?php echo(base_url()) ?>client/saran/add" method="post" id="addForm" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Saran/masukan anda</label>
                      <textarea id="editor1" name="content" rows="10" cols="80">
                      </textarea>
                    <input type="hidden" name="access" value="1">
                  </div>                                                      
                </div>
                <!-- /.box-body -->

                <div class="box-footer">   
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>                      
            </div>             
          </div>
        </div>
      	<div class="col-md-6" style="padding-top: 20px">
          <h4 class="box-title">Daftar Saran/Masukan Anda</h4>
          <div id="listSaran" style="padding: 5px;">

          </div>
       
      </div>
    </section>
</div>
<div class="modal fade" id="modal_delete">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dialog</h4>
              </div>
              <div class="modal-body">
                <h4 style="text-align: center;">Apakah anda yakin menghapus data ini?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary" data-id="" id="btn_modal">Ya</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div id="listWithItem" style="display: none;">
 
</div>
<div id="itemTemplate" style="display: none;">
                       <div class="col-md-12">
                          <div class="saran-box">
                            <div class="saran-header">
                              <div class="col-md-6">
                                <h5 id="tanggalBuat">Di inputkan pada : 20-06-2018</h5>  
                              </div>
                              <div class="col-md-6">
                                <h5 class="pull-right" id="tanggalUbah">Diubah pada : </h5>  
                              </div>
                            </div>
                            <div class="saran-separator"></div>
                            <div class="saran-body" id="contentSaran">
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                            <div class="saran-separator"></div>
                            <div class="saran-footer">
                              <a href="" class="pull-right" id="deleteItem" data-id=""><i class="fa fa-trash"></i></a>                              
                              <a href="" class="pull-right" id="editItem" data-id=""><i class="fa fa-pencil"></i></a>
                            </div>
                          </div>                      
                      </div>    
</div>
<div id="listNoItem" style="display: none;">
            <div class="col-md-12" style="text-align: center;">
              <h5>Tidak ada saran/masukan yang anda kirimkan</h5>
            </div>  
</div>
<div class="modal fade" id="manipulateModal">
          <div class="modal-dialog">
      <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title" id="modalTitle">Edit saran/masukan anda</h3>
              </div>
              <form action="<?php echo(base_url()) ?>client/saran/edit" method="post" id="manipulateForm" enctype="multipart/form-data">
                <div class="box-body">
                  <input type="hidden" id="idDetail" name="idDetail" value="">
                  <input type="hidden" name="access" value="1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Saran/masukan anda</label>
                      <textarea id="editor2" name="content" rows="10" cols="80">
                      </textarea>
                    <input type="hidden" name="access" value="1">
                  </div>                                                                   
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-success pull-right">Simpan</button>
                </div>
              </form>
            </div>              
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>