<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Invoicing
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo(base_url()); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Invoicing</li>
      </ol>
      <div class="col-xs-12 contentHeader" style="margin-top: 10px;"></div>
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-md-12" style="padding-top: 20px">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Invoice</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="tableInvoice" class="table table-bordered table-striped">
                    <thead>
                          <tr>
                            <th width="15%">Tanggal</th>
                            <th width="15%">No Invoice</th>
                            <th width="15%">Jatuh Tempo</th>
                            <th width="15%">Total</th>
                            <th width="20%">Catatan</th>
                            <th width="15%">Status</th>
                            <th width="105">Aksi</th>
                          </tr>
                    </thead>
                    <tbody id="listView">
                   

                  </table>
                </div>
                <!-- /.box-body -->
              </div>                       
            </div>             		
      	</div>
      </div>
    </section>
</div>
