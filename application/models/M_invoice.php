<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_invoice extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function list(){
			$this->db->select('tb_invoice.*,tb_client.nama_client');
			$this->db->from('tb_invoice');
			$this->db->join('tb_client','tb_invoice.id_client = tb_client.id_client');
			$this->db->where('tb_invoice.status !=','Cancel');
			$this->db->where('tb_invoice.status !=','Terbayar');
			return $this->db->get()->result();
		}
		function getid(){
			$this->db->order_by('no_invoice','desc');
			$data = $this->db->get('tb_invoice')->result();
			if(sizeof($data)>0){
			   $arr = explode("IV", $data[0]->no_invoice);
			   $id = $this->generateid($arr[1]+1); 
			} else {
			    $id = $this->generateid(1); 
			}
			
			return $id;
		}
		function generateid($id){
			$const = 5;
			$current = strlen($id);
			$result = "IV";
			for($i=0;$i<($const-$current);$i++){
				$result .="0";
			}
			$result .=$id;
			return $result; 

		}
		function insert($no_invoice,$id_client,$tanggal,$due,$total,$url,$note,$created_at){
			$data = array("no_invoice"=>$no_invoice,"id_client"=>$id_client,"tanggal_invoice"=>$tanggal,"due_date"=>$due,"total"=>$total,"file_invoice"=>$url,"note"=>$note,"created_at"=>$created_at,"status"=>"Belum dibayar");
			return $this->db->insert('tb_invoice',$data);
		}
		function detail($id){
			$this->db->where('id_invoice',$id);
			return $this->db->get('tb_invoice')->result();
		}
		function edit($id_invoice,$no_invoice,$id_client,$tanggal,$due,$total,$url,$note,$updated_at){
			$data = array("no_invoice"=>$no_invoice,"id_client"=>$id_client,"tanggal_invoice"=>$tanggal,"due_date"=>$due,"total"=>$total,"file_invoice"=>$url,"note"=>$note,"updated_at"=>$updated_at,"status"=>"Belum dibayar");
			$this->db->where('id_invoice',$id_invoice)			;
			return $this->db->update('tb_invoice',$data);
		}
		function delete($id){
			$this->db->where('id_invoice',$id);
			return $this->db->delete('tb_invoice');
		}
		function cancel($id){
			$data = array("status"=>"Cancel");
			$this->db->where('id_invoice',$id);
			return $this->db->update('tb_invoice',$data);
		}
		function archive(){
			$this->db->select('tb_invoice.*,tb_client.nama_client');
			$this->db->from('tb_invoice');
			$this->db->join('tb_client','tb_invoice.id_client = tb_client.id_client');
			$this->db->where('tb_invoice.status','Cancel');
			$this->db->or_where('tb_invoice.status','Terbayar');
			return $this->db->get()->result();
		}
		function clientList($id){
			$this->db->where('tb_invoice.status !=','Cancel');
			$this->db->where('tb_invoice.id_client',$id);
			return $this->db->get('tb_invoice')->result();
		}
		function clientNewList($id)	{
			$this->db->where('tb_invoice.status','Belum dibayar');
			$this->db->where('tb_invoice.id_client',$id);
			return $this->db->get('tb_invoice')->result();
		}
		function changeStatus($id,$status){
			$data = array("status"=>$status);
			$this->db->where('id_invoice',$id);
			return $this->db->update('tb_invoice',$data);
		}
		function receiptNewList()	{
			$this->db->select('tb_invoice.*');
			$this->db->from('tb_invoice');
			$this->db->join('tb_receipt','tb_invoice.id_invoice = tb_receipt.id_invoice','left');
			$this->db->where('tb_invoice.status',"Terbayar");
			$this->db->where('tb_receipt.id_invoice IS NULL', null, false);
			return $this->db->get()->result();
		}
		function allInvoice(){
			return $this->db->get('tb_invoice')->result();
		}
	}
?>