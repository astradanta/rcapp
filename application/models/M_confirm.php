<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_confirm extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function insert($id_invoice,$nama_bank,$atas_nama,$note,$bukti,$created_at){
			$data = array("id_invoice"=>$id_invoice,"nama_bank"=>$nama_bank,"atas_nama_bank"=>$atas_nama,"note"=>$note,"status_konfirmasi"=>"Menunggu verifikasi","file_bukti"=>$bukti,"created_at"=>$created_at);
			return $this->db->insert('tb_konfirmasi_pembayaran',$data);
		}
		function list($id){
			$this->db->select('tb_konfirmasi_pembayaran.*,tb_user.nama,tb_invoice.no_invoice');
			$this->db->from('tb_konfirmasi_pembayaran');
			$this->db->join('tb_invoice','tb_konfirmasi_pembayaran.id_invoice = tb_invoice.id_invoice');
			$this->db->join('tb_user','tb_user on tb_konfirmasi_pembayaran.id_user = tb_user.id_user','left');
			$this->db->where('tb_invoice.id_client',$id);
			return $this->db->get()->result();
		}
		function listAdminPanel(){
			$this->db->select('tb_konfirmasi_pembayaran.*,tb_client.nama_client,tb_invoice.no_invoice');
			$this->db->from('tb_konfirmasi_pembayaran');
			$this->db->join('tb_invoice','tb_konfirmasi_pembayaran.id_invoice = tb_invoice.id_invoice');
			$this->db->join('tb_client','tb_invoice.id_client = tb_client.id_client');
			return $this->db->get()->result();			
		}
		function detail($id){
			$this->db->where('id_konfirmasi',$id);
			$this->db->select('tb_konfirmasi_pembayaran.*,tb_user.nama,tb_invoice.no_invoice');
			$this->db->from('tb_konfirmasi_pembayaran');
			$this->db->join('tb_invoice','tb_konfirmasi_pembayaran.id_invoice = tb_invoice.id_invoice');
			$this->db->join('tb_user','tb_user on tb_konfirmasi_pembayaran.id_user = tb_user.id_user','left');
			return $this->db->get()->result();
		}
		 function edit($id_confirm,$id_invoice,$nama_bank,$atas_nama,$note,$bukti,$updated_at){
			$data = array("id_invoice"=>$id_invoice,"nama_bank"=>$nama_bank,"atas_nama_bank"=>$atas_nama,"note"=>$note,"status_konfirmasi"=>"Menunggu verifikasi","file_bukti"=>$bukti,"updated_at"=>$updated_at);
			$this->db->where('id_konfirmasi',$id_confirm);
			return $this->db->update('tb_konfirmasi_pembayaran',$data);		 	
		 }
		 function delete($id){
		 	$this->db->where('id_konfirmasi',$id);
		 	return $this->db->delete('tb_konfirmasi_pembayaran');
		 }
		 function changeStatus($id,$status,$id_user){
		 	$data = array("status_konfirmasi"=>$status,"id_user"=>$id_user);
		 	$this->db->where('id_konfirmasi',$id);
		 	return $this->db->update('tb_konfirmasi_pembayaran',$data);
		 }
		 function allConfirm(){
		 	$this->db->where('status_konfirmasi','Menunggu verifikasi');
		 	return $this->db->get('tb_konfirmasi_pembayaran')->result();
		 }
	}
?>