<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_user extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function getUserByEmail($email){
			$this->db->select('tb_user.*,tb_admin_role.role_name');
			$this->db->from('tb_user');
			$this->db->join('tb_admin_role','tb_user.id_role = tb_admin_role.id_role');
			$this->db->where('tb_user.email',$email);
			$this->db->where('tb_user.jenis_user',1);
			$query=$this->db->get();
			return $query->result();
		}

		function getClientByEmail($email){
			$this->db->where('email',$email);
			$this->db->where('jenis_user',0);
			$query=$this->db->get('tb_user');
			return $query->result();
		}
//
		function insertUsergetId($jenis,$nama,$email){
			$created_at = date('Y-m-d H:i:s');
			$data = array('jenis_user'=>$jenis,'nama'=>$nama,'email'=>$email,'created_at'=>$created_at);
			$this->db->insert('tb_user', $data);
		   	$insert_id = $this->db->insert_id();
		   	return  $insert_id;
		}
		function editUser($idUser,$jenis,$nama,$email){
			$updated_at = date('Y-m-d H:i:s');
			$data = array('jenis_user'=>$jenis,'nama'=>$nama,'email'=>$email,'updated_at'=>$updated_at);
			$this->db->where('id_user',$idUser);
			return $this->db->update('tb_user',$data);
		}

		function deleteUser($id){
			$this->db->where('id_user',$id);
			return $this->db->delete('tb_user');
		}

		function allUser(){
			$this->db->select('tb_user.*,tb_admin_role.role_name');
			$this->db->from('tb_user');
			$this->db->join('tb_admin_role','tb_user.id_role = tb_admin_role.id_role','left');
			$query=$this->db->get();
			return $query->result();
		}
		function insertUser($nama,$email,$password,$jenis,$created_at,$logo,$id_role){
			$data = array("nama"=>$nama,"email"=>$email,"jenis_user"=>$jenis,"created_at"=>$created_at,"password"=>$password,"photo"=>$logo,"id_role"=>$id_role);
			return $this->db->insert("tb_user",$data);
		}
		function getDetail($id){
			$this->db->select('tb_user.*,tb_admin_role.role_name');
			$this->db->from('tb_user');
			$this->db->join('tb_admin_role','tb_user.id_role = tb_admin_role.id_role','left');			
			$this->db->where('tb_user.id_user',$id);
			return $this->db->get()->result();
		}
		function editUserWithPassword($id_user,$nama,$email,$password,$jenis,$updated_at,$photo,$id_role){
			$data = array("nama"=>$nama,"email"=>$email,"jenis_user"=>$jenis,"updated_at"=>$updated_at,"password"=>$password,"photo"=>$photo,"id_role"=>$id_role);
			$this->db->where('id_user',$id_user);
			return $this->db->update("tb_user",$data);			
		}
		function allRole(){
			return $this->db->get('tb_admin_role')->result();
		}
	}
?>