<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_pekerjaan_client extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('m_user','',TRUE);
		}

		function getListPekerjaan($id){
			$this->db->select('tb_kerja.*,tb_daftar_pekerjaan.nama_pekerjaan,tb_daftar_pekerjaan_client.id_pekerjaan, r_kerja_detail_kerja.total_kerja,r_kerja_detail_kerja.total_dikerjakan ');
			$this->db->from('tb_kerja');
			$this->db->join('tb_daftar_pekerjaan_client','tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client');
			$this->db->join('tb_daftar_pekerjaan','tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			$this->db->join('r_kerja_detail_kerja','tb_kerja.id_kerja = r_kerja_detail_kerja.id_kerja');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id);
			$this->db->where('tb_kerja.status',"on progress");
			if(isset($_SESSION['role_id'])){
    			if(($_SESSION['role_id']==3)||($_SESSION['role_id']==4)){
    			$this->db->where('tb_daftar_pekerjaan_client.id_user',$_SESSION['id']);}
    			//echo($this->db->_compile_select());
			}
			return $this->db->get()->result();
		}
		function listPekerjaanKlien($id){
			$this->db->select('tb_daftar_pekerjaan.nama_pekerjaan, tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client');
			$this->db->from('tb_daftar_pekerjaan_client');
			$this->db->join('tb_daftar_pekerjaan','tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id);
			if(($_SESSION['role_id']==3)||($_SESSION['role_id']==4)){
			$this->db->where('tb_daftar_pekerjaan_client.id_user',$_SESSION['id']);}			
			return $this->db->get()->result();
		}
		function getPekerjaanKlien($id){
			$sql = 'SELECT a.*,if(b.id_user is null,0,b.id_user) as pic,b.id_daftar_pekerjaan_client from tb_daftar_pekerjaan a LEFT JOIN (SELECT tb_daftar_pekerjaan_client.* FROM tb_daftar_pekerjaan_client WHERE tb_daftar_pekerjaan_client.id_client = '.$id.') b on a.id_pekerjaan = b.id_pekerjaan';
			$query = $this->db->query($sql);
			return $query->result();			

		}
		function insert($idPekerjaan,$startDate,$deadLine,$created_at){
			$data = array("id_daftar_pekerjaan_client"=>$idPekerjaan,"start_date"=>$startDate,"deadline"=>$deadLine,"created_at"=>$created_at,"status"=>"on progress");
			return $this->db->insert('tb_kerja',$data);
		}
		function detail($id){
			$this->db->where('id_kerja',$id);
			return $this->db->get('tb_kerja')->result();
		}
		function edit($idDetail,$pekerjaan,$startDate,$deadLine,$updated_at){
			$data = array("id_daftar_pekerjaan_client"=>$pekerjaan,"start_date"=>$startDate,"deadline"=>$deadLine,"updated_at"=>$updated_at);	
			$this->db->where('id_kerja',$idDetail);
			return $this->db->update('tb_kerja',$data);
		}
		function delete($id){
			$this->db->where("id_kerja",$id);
			return $this->db->delete('tb_kerja');
		}
		function getOnProgress($id_client,$id_pekerjaan){
			$this->db->select('tb_kerja.*');
			$this->db->from('tb_kerja');
			$this->db->join('tb_daftar_pekerjaan_client','tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id_client);
			$this->db->where('tb_daftar_pekerjaan_client.id_pekerjaan',$id_pekerjaan);
			$this->db->where('tb_kerja.status','on progress');
			return $this->db->get()->result();
		}
		function getArchive($id_client,$id_pekerjaan){
			$this->db->select('tb_kerja.*');
			$this->db->from('tb_kerja');
			$this->db->join('tb_daftar_pekerjaan_client','tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client = tb_kerja.id_daftar_pekerjaan_client');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id_client);
			$this->db->where('tb_daftar_pekerjaan_client.id_pekerjaan',$id_pekerjaan);
			$this->db->where('tb_kerja.status','selesai');
			return $this->db->get()->result();
		}
		function getItemOnProgress($id_kerja,$id_pekerjaan){
			$sql = 'SELECT a.*,b.upload_file,b.id_detail_kerja from tb_detail_pekerjaan a left JOIN (SELECT * from tb_detail_kerja WHERE tb_detail_kerja.id_kerja = '.$id_kerja.')  b on a.id_detail_pekerjaan = b.id_detail_pekerjaan WHERE a.id_pekerjaan = '.$id_pekerjaan;
			$query = $this->db->query($sql);
			return $query->result();
		}
		function insert_tb_detail_kerja($id_kerja,$id_detail_pekerjaan,$url,$created_at){
			$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>$id_detail_pekerjaan,"upload_file"=>$url,"created_at"=>$created_at);
			return $this->db->insert('tb_detail_kerja',$data);
		}
		function edit_tb_detail_kerja($id_detail_kerja,$id_kerja,$id_detail_pekerjaan,$url,$updated_at,$updated_by){
			$data = array("id_kerja"=>$id_kerja,"id_detail_pekerjaan"=>$id_detail_pekerjaan,"upload_file"=>$url,"updated_at"=>$updated_at,"updated_by"=>$updated_by);
			$this->db->where('id_detail_kerja',$id_detail_kerja);

			return $this->db->update('tb_detail_kerja',$data);			
		}
		// function getHistoryPekerjaan(){
		// 	$sql = 'SELECT tb_history_kerja.*,tb_daftar_pekerjaan.nama_pekerjaan,tb_client.nama_client,tb_detail_pekerjaan.nama_detail_pekerjaan,tb_detail_kerja.created_at as "upload_date", tb_detail_kerja.updated_at as "reupload_date", tb_detail_kerja.upload_file from tb_history_kerja INNER JOIN tb_kerja on tb_history_kerja.id_kerja = tb_kerja.id_kerja INNER JOIN tb_detail_kerja on tb_detail_kerja.id_kerja = tb_kerja.id_kerja INNER JOIN tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_daftar_pekerjaan on tb_daftar_pekerjaan.id_pekerjaan = tb_daftar_pekerjaan_client.id_pekerjaan INNER JOIN tb_client on tb_client.id_client = tb_daftar_pekerjaan_client.id_client INNER join tb_detail_pekerjaan on tb_detail_pekerjaan.id_detail_pekerjaan = tb_detail_kerja.id_detail_pekerjaan';
		// 	$query = $this->db->query($sql);
		// 	return $query->result();
		// }
		function getHistoryPekerjaan(){
			$sql = 'SELECT tb_history_kerja.*,tb_client.nama_client,tb_client.id_client,tb_daftar_pekerjaan.nama_pekerjaan from tb_history_kerja inner join tb_kerja on tb_history_kerja.id_kerja = tb_kerja.id_kerja inner join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER join tb_client on tb_daftar_pekerjaan_client.id_client = tb_client.id_client INNER join tb_daftar_pekerjaan on tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan';
			$query = $this->db->query($sql);
			return $query->result();
		}
		function getDetailHistoryPekerjaan($id){
			$sql = 'SELECT tb_history_kerja.*,tb_client.nama_client,tb_client.id_client,tb_daftar_pekerjaan.nama_pekerjaan,tb_daftar_pekerjaan.id_pekerjaan from tb_history_kerja inner join tb_kerja on tb_history_kerja.id_kerja = tb_kerja.id_kerja inner join tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER join tb_client on tb_daftar_pekerjaan_client.id_client = tb_client.id_client INNER join tb_daftar_pekerjaan on tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan where tb_history_kerja.id_history_kerja = '.$id;
			$query = $this->db->query($sql);
			return $query->result();
		}		
		function searchtHistoryPekerjaan($keyword){
			$sql = 'SELECT tb_history_kerja.*,tb_daftar_pekerjaan.nama_pekerjaan,tb_client.nama_client,tb_detail_pekerjaan.nama_detail_pekerjaan,tb_detail_kerja.created_at as "upload_date", tb_detail_kerja.updated_at as "reupload_date", tb_detail_kerja.upload_file from tb_history_kerja INNER JOIN tb_kerja on tb_history_kerja.id_kerja = tb_kerja.id_kerja INNER JOIN tb_detail_kerja on tb_detail_kerja.id_kerja = tb_kerja.id_kerja INNER JOIN tb_daftar_pekerjaan_client on tb_kerja.id_daftar_pekerjaan_client = tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client INNER JOIN tb_daftar_pekerjaan on tb_daftar_pekerjaan.id_pekerjaan = tb_daftar_pekerjaan_client.id_pekerjaan INNER JOIN tb_client on tb_client.id_client = tb_daftar_pekerjaan_client.id_client INNER join tb_detail_pekerjaan on tb_detail_pekerjaan.id_detail_pekerjaan = tb_detail_kerja.id_detail_pekerjaan where tb_client.nama_client like "%'.$keyword.'%"';
			$query = $this->db->query($sql);
			return $query->result();
		}
		function changeStatus($id){
			$data = array("tb_kerja.status"=>"selesai");
			$this->db->where('tb_kerja.id_kerja',$id);
			$update = $this->db->update('tb_kerja',$data);
			$this->insertHistory($id);
			return $update;
		}
		function insertHistory($id){
			$created_at = date('Y-m-d h:i:s');
			$created_by = $_SESSION['nama'];
			$data = array("id_kerja"=>$id,"created_at"=>$created_at,"created_by"=>$created_by);
			return $this->db->insert('tb_history_kerja',$data);
		}
		function allKerja(){
			$this->db->where('status','on progress');
			return $this->db->get('tb_kerja')->result();
		}
	}
?>