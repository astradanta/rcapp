<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_client extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('m_user','',TRUE);
		}

		function getClientList(){
			if(($_SESSION['role_id'] == 3) || ($_SESSION['role_id'] == 5)){
				$this->db->like('tipe_kerja',',Tax,');
				if($_SESSION['role_id'] == 3){
					$this->db->like('list_pic',','.$_SESSION['id'].',');
				}
			}
			if(($_SESSION['role_id'] == 4) || ($_SESSION['role_id'] == 6)){
				$this->db->like('tipe_kerja',',Finance,');
				if($_SESSION['role_id'] == 4){
					$this->db->like('list_pic',','.$_SESSION['id'].',');
				}				
			}
			$query=$this->db->get('r_pekerjaan_client');
			return $query->result();
		}
//
		function filterClient($id,$keyword){
			if ($id > 0 ){
				$this->db->like('list_kerja',','.$id.',');
			} else {
				$this->db->like('list_kerja',',');
			}
			$this->db->group_start();
			$this->db->like('nama',$keyword);
			$this->db->or_like('count',$keyword);
			$this->db->group_end();
			$query=$this->db->get('r_pekerjaan_client');
			return $query->result();
		}
		function insertClient($nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo = ''){
			//
			$idUser = $this->m_user->insertUsergetId(0,$nama,$email);
			$created_at = date('Y-m-d H:i:s');
			$data = array("id_user"=>$idUser,"nama_client"=>$nama,"alamat"=>$alamat,"no_telepon"=>$telepon,"email"=>$email,"deskripsi"=>$deskripsi,"logo_client"=>$logo,"start_contract"=>$startContract,"end_contract"=>$endContract,"jenis_perusahaan"=>$jenis,"created_at"=>$created_at);
			
		   	return  $this->db->insert('tb_client', $data);

		}

		function insertPekerjaanClient($idClient,$pekerjaan,$pic){
			$created_at = date('Y-m-d H:i:s');
			$data = ['id_client'=>$idClient,'id_pekerjaan'=>$pekerjaan,'created_at'=>$created_at,'id_user'=>$pic];
			return $this->db->insert('tb_daftar_pekerjaan_client',$data);
		}
		function editPekerjaanClient($id,$idClient,$pekerjaan,$pic){
			$updated_at = date('Y-m-d H:i:s');
			$data = ['id_client'=>$idClient,'id_pekerjaan'=>$pekerjaan,'updated_at'=>$updated_at,'id_user'=>$pic];
			$this->db->where('tb_daftar_pekerjaan_client.id_daftar_pekerjaan_client',$id);
			return $this->db->update('tb_daftar_pekerjaan_client',$data);
		}

		function deletePekerjaanClient($idClient,$pekerjaan){
			$this->db->where('id_client',$idClient);
			$this->db->group_start();
			foreach ($pekerjaan as $key) {
				$this->db->or_where('id_pekerjaan',$key);
			}
			$this->db->group_end();
			return $this->db->delete('tb_daftar_pekerjaan_client');
		}

		function getDetailClient($id){ 
			$this->db->select('tb_client.*,r_pekerjaan_client.list_kerja');
			$this->db->from('tb_client');
			$this->db->join('r_pekerjaan_client','tb_client.id_client = r_pekerjaan_client.id');
			$this->db->where('tb_client.id_client',$id);
			return $this->db->get()->result();
		}
		function editClient($idClient,$idUser,$nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo){
			$this->m_user->editUser($idUser,0,$nama,$email);
			$updated_at = date('Y-m-d H:i:s');
			$updated_by = $_SESSION['nama'];
			$data = array("id_user"=>$idUser,"nama_client"=>$nama,"alamat"=>$alamat,"no_telepon"=>$telepon,"email"=>$email,"deskripsi"=>$deskripsi,"logo_client"=>$logo,"start_contract"=>$startContract,"end_contract"=>$endContract,"jenis_perusahaan"=>$jenis,"updated_at"=>$updated_at,'updated_by'=>$updated_by);
			$this->db->where('id_client',$idClient);
			return $this->db->update('tb_client', $data);
		}
		function getPekerjaanClient($id){
			$this->db->select('tb_daftar_pekerjaan_client.*, tb_daftar_pekerjaan.nama_pekerjaan');//
			$this->db->from('tb_daftar_pekerjaan_client');
			$this->db->join('tb_daftar_pekerjaan','tb_daftar_pekerjaan_client.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			$this->db->where('tb_daftar_pekerjaan_client.id_client',$id);
			return $this->db->get()->result();
		}
		function getClientByUser($id){
			$this->db->where('id_user',$id);
			return $this->db->get('tb_client')->result();
		}
	}
?>