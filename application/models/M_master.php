<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_master extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function getListPekerjaan(){
			$query=$this->db->get('tb_daftar_pekerjaan');
			return $query->result();
		}

		function insertPekerjaan($nama,$deskripsi,$created_at,$kategori,$tipe){
			$data = array("nama_pekerjaan"=>$nama,"deskripsi_pekerjaan"=>$deskripsi,"created_at"=>$created_at,"kategori"=>$kategori,"tipe"=>$tipe);
			return $this->db->insert('tb_daftar_pekerjaan',$data);
		}
		function getDetailDataPekerjaan($id){
			$this->db->where('id_pekerjaan',$id);
			return $this->db->get('tb_daftar_pekerjaan')->result();
		}
		function editPekerjaan($id_pekerjaan,$nama,$deskripsi,$updated_at,$updated_by,$kategori,$tipe){
			$this->db->where('id_pekerjaan',$id_pekerjaan);
			$data = array("nama_pekerjaan"=>$nama,"deskripsi_pekerjaan"=>$deskripsi,"updated_at"=>$updated_at,"updated_by"=>$updated_by,"kategori"=>$kategori,"tipe"=>$tipe);
			return $this->db->update('tb_daftar_pekerjaan',$data);
		}
		function deletePekerjaan($id){
			$this->db->where('id_pekerjaan',$id);
			return $this->db->delete('tb_daftar_pekerjaan');
		}
		function getAllDetailPekerjaan(){
			$this->db->select('tb_detail_pekerjaan.*,tb_daftar_pekerjaan.nama_pekerjaan');
			$this->db->from('tb_detail_pekerjaan');
			$this->db->join('tb_daftar_pekerjaan','tb_detail_pekerjaan.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			$this->db->order_by("tb_daftar_pekerjaan.nama_pekerjaan");
			return $this->db->get()->result();
		}
		function insertDetail($nama,$deskripsi,$created_at,$idPekerjaan,$jenis_pekerjaan){
			$data = array("nama_detail_pekerjaan"=>$nama,"deskripsi"=>$deskripsi,"created_at"=>$created_at,"id_pekerjaan"=>$idPekerjaan,"jenis_pekerjaan"=>$jenis_pekerjaan);
			return $this->db->insert('tb_detail_pekerjaan',$data);
		}
		function spesificDetailPekerjaan($id){
			$this->db->select('tb_detail_pekerjaan.*,tb_daftar_pekerjaan.nama_pekerjaan');
			$this->db->from('tb_detail_pekerjaan');
			$this->db->join('tb_daftar_pekerjaan','tb_detail_pekerjaan.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			$this->db->where('id_detail_pekerjaan',$id);
			return $this->db->get('')->result();
		}
		function editSpesificDetail($id_detail_pekerjaan,$nama,$deskripsi,$idPekerjaan,$jenis_pekerjaan,$updated_at,$updated_by){
			$data = array("nama_detail_pekerjaan"=>$nama,"deskripsi"=>$deskripsi,"updated_at"=>$updated_at,"id_pekerjaan"=>$idPekerjaan,"jenis_pekerjaan"=>$jenis_pekerjaan,"updated_by"=>$updated_by);
			$this->db->where('id_detail_pekerjaan',$id_detail_pekerjaan);
			return $this->db->update('tb_detail_pekerjaan',$data);
		}
		function deleteSpesificPekerjaan($id){
			$this->db->where('id_detail_pekerjaan',$id);
			return $this->db->delete('tb_detail_pekerjaan');			
		}	
		function countPekerjaan($key = "all"){
			if ($key != "all"){
				$this->db->where('tb_daftar_pekerjaan.tipe',$key);
			}
			$this->db->select('r_kerja_detail_kerja.*,tb_daftar_pekerjaan.tipe');
			$this->db->from('r_kerja_detail_kerja');
			$this->db->join('tb_daftar_pekerjaan',' r_kerja_detail_kerja.id_pekerjaan = tb_daftar_pekerjaan.id_pekerjaan');
			return $this->db->get()->result();
		}	
	}
?>