<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_invoice','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_confirm','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/home2');
		$this->load->view('Back_office/static/footer2');
	}
	function logout(){
		$date = date('Y-m-d');
		$this->m_log->insertLog($_SESSION['id'],'Logout',$date);
		unset($_SESSION['nama']);
		unset($_SESSION['status']);
		unset($_SESSION['email']);
		$result = array();
		$result['status'] = 1;
		$result['message'] = "Anda berhasil keluar";

		echo json_encode($result);
	}
	function listClient(){
		$data = $this->m_client->getClientList();
		$result = array();
		foreach ($data as $key) {
			$tempData['id'] = $key->id;
			$tempData['nama'] = $key->nama;
			$tempData['list_kerja'] = substr($key->list_kerja, 1,strlen($key->list_kerja)-2) ;
			$tempData['countKerja'] = $key->count;
			array_push($result, $tempData);
		}
		echo json_encode($result);
	}
	function listPekerjaan(){
		$data = $this->m_master->getListPekerjaan();
		$result = array();
		foreach ($data as $key) {
			$tempData['id'] = $key->id_pekerjaan;
			$tempData['nama'] = $key->nama_pekerjaan;
			array_push($result, $tempData);
		}
		echo json_encode($result);		
	}
	function filterClient(){
		if(isset($_POST['id'])&&(isset($_POST['keyword']))){
			$id = $_POST['id'];
			$keyword = $_POST['keyword'];
			$data = $this->m_client->filterClient($id,$keyword);
			$result = array();
			foreach ($data as $key) {
				$tempData['id'] = $key->id;
				$tempData['nama'] = $key->nama;
				$tempData['list_kerja'] = substr($key->list_kerja, 1,strlen($key->list_kerja)-2) ;
				$tempData['countKerja'] = $key->count;
				array_push($result, $tempData);
			}
			echo json_encode($result);
		}
		
	}
function addClient(){
		$result['status'] = 'failed';
		if (isset($_POST['btn_save'])){
			$nama = $_POST['nama'];
			$jenis = $_POST['jenis'];
			$alamat = $_POST['alamat'];
			$telepon = $_POST['telepon'];
			$email = $_POST['email'];
			$deskripsi = $_POST['deskripsi'];
			$time = strtotime($_POST['startContract']);
			$startContract = date('Y-m-d',$time);
			$time = strtotime($_POST['endContract']);
			$endContract = date('Y-m-d',$time);
			$logo = '';
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
			}
			$insert = $this->m_client->insertClient($nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah Client '.$nama,$date);
			}

		}
		echo json_encode($result);
	}

	function uploadLogo($url){
				$date = new DateTime();
				$config['file_name']          = $date->getTimestamp().random_string('alnum', 5);
                $config['upload_path']          = 'assets/img/client/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 2000;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
                return $url;
	}

	function detailClient(){
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$data = $this->m_client->getDetailClient($id);
			echo json_encode($data);
		}
	}
	function editClient(){
		$result['status'] = 'failed';
		if(isset($_POST['idClient'])){
			$idClient = $_POST['idClient'];
			$nama = $_POST['nama'];
			$jenis = $_POST['jenis'];
			$alamat = $_POST['alamat'];
			$telepon = $_POST['telepon'];
			$email = $_POST['email'];
			$deskripsi = $_POST['deskripsi'];
			$time = strtotime($_POST['startContract']);
			$startContract = date('Y-m-d',$time);
			$time = strtotime($_POST['endContract']);
			$endContract = date('Y-m-d',$time);
			$data = $this->m_client->getDetailClient($idClient);
			$oldLogo = $data[0]->logo_client;
			$idUser = $data[0]->id_user;
			$list_kerja = substr($data[0]->list_kerja, 1,strlen($data[0]->list_kerja)-2) ;
			$logo = $oldLogo;			
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
				if ($logo != $oldLogo){
					if(file_exists(base_url().$oldLogo)){
						unlink(base_url().$oldLogo);
					}
				}
			}
			$edit = $this->m_client->editClient($idClient,$idUser,$nama,$jenis,$alamat,$telepon,$email,$deskripsi,$startContract,$endContract,$logo);
			if ($edit){
				$result['status'] = 'success';
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Client '.$nama,$date);
			}
		}
		echo json_encode($result);
	}
	function deleteClient(){
		$result["status"] = "faild";
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$data = $this->m_client->getDetailClient($id);
			$idUser = $data[0]->id_user;
			$delete = $this->m_user->deleteUser($idUser);
			if($delete){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Client '.$data[0]->nama_client,$date);
			}
		}
		echo json_encode($result);
	}
	function countElement(){
		$client = $this->m_client->getClientList();
		$counCLient = sizeof($client);
		$invoice = $this->m_invoice->allInvoice();
		$finishInvoice = $this->m_invoice->archive();
		$countInvoice =sizeof($invoice);
		$kerja = $this->m_pekerjaan_client->allKerja();
		$counKerja = sizeof($kerja);
		$confirm = $this->m_confirm->allConfirm();
		$countConfirm = sizeof($confirm);
		$result['client'] = $counCLient;
		$result['invoice'] = $countInvoice;
		$result['kerja'] = $counKerja;
		$result['confirm'] = $countConfirm;
		echo(json_encode($result));
	}
}