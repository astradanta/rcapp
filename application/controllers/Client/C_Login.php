<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->library('email');
	}

	public function index()
	{
		$this->load->view('Client/static/headerLogin');
		$this->load->view('Client/login');
		$this->load->view('Client/static/footerLogin');
		if(isset($_SESSION['namaClient'])){
			redirect(base_url().'client');
		}
	}
	function signIn(){
		$result = array();
		$result['status'] = 0;
		$result['message'] = 'Kekurangan Beberapa Parameter Fungsi';
		if (isset($_POST['email']) && isset($_POST['password'])) {
			$email = $_POST['email'];
			$password = $_POST['password'];
			$data = $this->m_user->getClientByEmail($email);
			if (sizeof($data) == 0) {
				$result['message'] = "Anda Belum Terdaftar";
			} else {
				$dataStatus = $data[0]->jenis_user;
				$dataPassword = $this->encryption->decrypt($data[0]->password);
				//echo $dataPassword;
				if ($dataStatus > 0) {
					$result['message'] = "Anda tidak memiliki akses pada halaman ini";
				}else if($password != $dataPassword){
					$result['message'] = 'Password anda salah';
				} else {
					$result['status'] = 1;
					$result['message'] = 'Login Berhasil';
					$_SESSION['namaClient'] = $data[0]->nama;
					$_SESSION['emailClient'] = $email;
					$_SESSION['statusClient'] = $dataStatus;
					$_SESSION['idClient'] = $data[0]->id_user;
					$client = $this->m_client->getClientByUser($data[0]->id_user);
					$_SESSION['photoClient'] = base_url().$client[0]->logo_client;
					$_SESSION['id_client'] = $client[0]->id_client;
					$date = date('Y-m-d');
					$this->m_log->insertLog($data[0]->id_user,'Login',$date);
				}
			}
		}
		echo json_encode($result);
	}

	function forget(){
		$result = array();
		$result['status'] = 0;
		$result['message'] = 'Kekurangan Beberapa Parameter Fungsi';
		if (isset($_POST['email'])){
			$email = $_POST['email'];
			 $data = $this->m_user->getClientByEmail($email);
			 if (sizeof($data) == 0) {
				$result['message'] = "Anda Belum Terdaftar";
			} else {
				$dataStatus = $data[0]->jenis_user;
				if ($dataStatus > 0) {
					$result['message'] = "Anda tidak memiliki akses pada halaman ini";
				} else {
					$result['status'] = 1;
					$result['message'] = 'Pengiriman berhasil, silakan cek email anda';
						$name = $data[0]->nama;
					$password = $this->encryption->decrypt($data[0]->password);
                     $this->sendPasswordWithEmail($name,$email,"redconsulting@gmail.com","RED Consulting",$password);
				}
			}
		}
		echo json_encode($result);	

	}
	function sendPasswordWithEmail($name,$to,$from,$fromName,$password){
        $htmlmessage="<!DOCTYPE html> 
						<head> 
					    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
						</head>
						<body> 

						<table cellpadding='10' style='border-color:#666' rules='all'>
							<tbody>
								<tr style='background-color:#393e40;color:white;font-size:14px'>
								<td colspan='2'></td>
								</tr>
								<tr>
									<td><p><MyFont12>Email</MyFont12></p></td>
									<td style='width:400px;'><p><MyFont12>$to</MyFont12></p></td>
								</tr>
								
								<tr>
									<td><p><MyFont12>Password</MyFont12></p></td>
									<td><p><MyFont12>$password</MyFont12></p></td>
								</tr>
							</tbody>
						</table>
					 <br> <br>
									
							  </body>
							  </html>

					";
		$this->email->from($from, 'RED Consulting');
		$this->email->to($to);
		$this->email->subject('Your Account Access');
		$this->email->set_mailtype("html");
		$this->email->message($htmlmessage);

		$this->email->send();
	}
}
