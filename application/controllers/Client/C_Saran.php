<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Saran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_saran','',TRUE);
		$this->load->model('m_client','',TRUE);

	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/saran');
		$this->load->view('Client/static/footer');			
	}
	function list(){
		if (isset($_POST['baselink'])){
			$idUser = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($idUser);
			$idClient = $client[0]->id_client;
			$saran = $this->m_saran->saranClient($idClient);
			$result['count'] = sizeof($saran);
			foreach ($saran as $key) {
				$time = strtotime($key->create_at);
				$key->create_at = date('d-m-Y',$time);
				$time = strtotime($key->update_at);
				if($key->update_at == null){
					$key->update_at = "";
				} else {
					$key->update_at = date('d-m-Y',$time);
				}
			}
			$result['data'] = $saran;
			echo(json_encode($result));
		}
	}
	function edit(){
		$result['status'] = 'failed';
		if (isset($_POST['access'])){
			$id = $_POST['idDetail'];
			$content = strip_tags($_POST['content']);
			$update_at = date('Y-m-d h:i:s');
			$edit = $this->m_saran->edit($id,$content,$update_at);
			if($edit){
				$result['status'] = 'success';
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Mengubah konten saran',$date);					
			}
		}
		echo(json_encode($result));
	}
	function add(){
		$result['status'] = 'failed';
		if (isset($_POST['access'])){
			$content = strip_tags($_POST['content']);
			$create_at = date('Y-m-d h:i:s');
			$idUser = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($idUser);
			$idClient = $client[0]->id_client;
			$edit = $this->m_saran->add($idClient,$content,$create_at);
			if($edit){
				$result['status'] = 'success';
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Manmbahkan saran',$date);					
			}
		}
		echo(json_encode($result));
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$delete = $this->m_saran->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Hapus saran',$date);				
			}
		}
		echo json_encode($result);		
	}
}
