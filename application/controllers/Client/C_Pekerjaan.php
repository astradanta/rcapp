<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pekerjaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);		
	}

	public function index()
	{
		$id_client = $this->uri->segment(4);
		$id_pekerjaan = $this->uri->segment(5);
		$data["id_client"] = $id_client;
		$data["id_pekerjaan"] = $id_pekerjaan;
		$temp = $this->m_master->getDetailDataPekerjaan($data["id_pekerjaan"]);
		$data["namaPekerjaan"] = $temp[0]->nama_pekerjaan;
		$this->load->view('Client/static/header',$data);
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/pekerjaan');
		$this->load->view('Client/static/footer');			
	}
	function getOnProgress(){
		if(isset($_POST['id_client'])&&isset($_POST['id_pekerjaan'])){
			$id_client = $_POST['id_client'];
			$id_pekerjaan = $_POST['id_pekerjaan'];
			$data = $this->m_pekerjaan_client->getOnProgress($id_client,$id_pekerjaan);
			$generalData['count'] = sizeof($data);
			if ($generalData['count']> 0){
				$generalData['start_date'] = $data[0]->start_date;
				$generalData['deadline'] = $data[0]->deadline;
				$generalData['status'] = $data[0]->status;
				$generalData['id_kerja'] = $data[0]->id_kerja;
				$itemData = $this->m_pekerjaan_client->getItemOnProgress($data[0]->id_kerja,$id_pekerjaan);
				$generalData['itemData'] = $itemData;
			}
			echo json_encode($generalData);
		} else {
			var_dump($_POST);
		}	
	}
	function getArchive(){
		if(isset($_POST['id_client'])&&isset($_POST['id_pekerjaan'])){
			$id_client = $_POST['id_client'];
			$id_pekerjaan = $_POST['id_pekerjaan'];
			$data = $this->m_pekerjaan_client->getArchive($id_client,$id_pekerjaan);
			$generalData['count'] = sizeof($data);
			if ($generalData['count']> 0){
				$generalData['start_date'] = $data[0]->start_date;
				$generalData['deadline'] = $data[0]->deadline;
				$generalData['status'] = $data[0]->status;
				$generalData['id_kerja'] = $data[0]->id_kerja;
				$itemData = $this->m_pekerjaan_client->getItemOnProgress($data[0]->id_kerja,$id_pekerjaan);
				foreach ($itemData as $key) {
					$time = strtotime($key->created_at);
					$key->created_at = date('d-m-Y',$time);
					$time = strtotime($key->updated_at);					
					if($key->updated_at == null){
						$key->updated_at = "";
					}else{
						$key->updated_at = date('d-m-Y',$time);
					} 
					if($key->updated_by == null){
						$key->updated_by = "";
					}
				}
				$generalData['itemData'] = $itemData;
			}
			echo json_encode($generalData);
		}	
	}
	function add(){
		$result["status"] = "failed";
		if (isset($_POST['access'])) {
			$created_at = date('Y-m-d h:i:s');
			$id_kerja = $_POST['id_kerja'];
			$id_detail_pekerjaan = $_POST['id_pekerjaan'];
			$url = $_POST['url'];
			$insert = $this->m_pekerjaan_client->insert_tb_detail_kerja($id_kerja,$id_detail_pekerjaan,$url,$created_at);
			$data = $this->m_master->spesificDetailPekerjaan($id_detail_pekerjaan);
			if($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Upload '.$data[0]->nama_detail_pekerjaan,$date);
			}
		}
		echo json_encode($result);
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_detail_kerja = $_POST['idDetail'];
			$id_kerja = $_POST['id_kerja'];
			$id_detail_pekerjaan = $_POST['id_pekerjaan'];
			$url = $_POST['url'];			
			$updated_at = date('Y-m-d h:i:s');
			$updated_by = $_SESSION["namaClient"];
			$edit = $this->m_pekerjaan_client->edit_tb_detail_kerja($id_detail_kerja,$id_kerja,$id_detail_pekerjaan,$url,$updated_at,$updated_by);
			$data = $this->m_master->spesificDetailPekerjaan($id_detail_pekerjaan);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Ulangi upload '.$data[0]->nama_detail_pekerjaan,$date);				
			}
		}
		echo json_encode($result);		
	}
}
