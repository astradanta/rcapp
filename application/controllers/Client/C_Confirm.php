<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Confirm extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_invoice','',TRUE)		;
		$this->load->model('m_confirm','',TRUE);
		$this->load->model('m_receipt','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->helper('string');
	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/confirm');
		$this->load->view('Client/static/footer');			
	}
	function list()	{
		if (isset($_POST['baselink'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);			
			$data = $this->m_confirm->list($client[0]->id_client);
			foreach ($data as $key ) {
				if($key->nama == null){
					$key->nama = "";
				}
				$key->file_bukti = base_url().$key->file_bukti;
			}
			echo json_encode($data);
		}
	}
	function add(){
		$result['status'] = "failed";
		if (isset($_POST['access'])){
			$id_invoice = $_POST['pekerjaan'];
			$nama_bank = $_POST['namaBank'];
			$atas_nama = $_POST['atasNamaBank'];
			$note = $_POST['note'];
			$created_at = date('Y-m-d h:i:s');
			$bukti = '';
			if ($_FILES['bukti']['name'] != ""){
				$bukti = $this->uploadLogo($bukti);
			}
			$insert = $this->m_confirm->insert($id_invoice,$nama_bank,$atas_nama,$note,$bukti,$created_at);
			if($insert){
				$change = $this->m_invoice->changeStatus($id_invoice,'Verifikasi pembayaran');
				$result['status'] = "success";
			}
		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_POST['baselink'])){
			$id = $_POST["id"];
			$data = $this->m_confirm->detail($id);
			echo json_encode($data);
		}
		
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_confirm = $_POST['idDetail'];
			$oldData = $this->m_confirm->detail($id_confirm);
			$id_invoice = $_POST['pekerjaan'];
			$nama_bank = $_POST['namaBank'];
			$atas_nama = $_POST['atasNamaBank'];
			$note = $_POST['note'];
			$bukti = $_POST['bukti'];
			$updated_at = date('Y-m-d h:i:s');
			$edit = $this->m_confirm->edit($id_confirm,$id_invoice,$nama_bank,$atas_nama,$note,$bukti,$updated_at);
			if ($edit){
				$result["status"] = "success";
				if($oldData[0]->id_invoice != $id_invoice){
					$this->m_invoice->changeStatus($oldData[0]->id_invoice,'Belum dibayar');
					$this->m_invoice->changeStatus($id_invoice,'Verifikasi pembayaran');
				}
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['idClient'],'Ubah Konfirmasi Pembayaran Invoice  '.$oldData[0]->no_invoice,$date);				
			}
		}
		echo json_encode($result);		
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_confirm->detail($id);
			$delete = $this->m_confirm->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$this->m_invoice->changeStatus($data[0]->id_invoice,'Belum dibayar');
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus konfirmasi pembayaran invoice  '.$data[0]->no_invoice,$date);				
			}
		}
		echo json_encode($result);
	}		
	function listReceipt(){
		if (isset($_POST['baselink'])){
			$id = $_SESSION['idClient'];
			$client = $this->m_client->getClientByUser($id);			
			$data = $this->m_receipt->listReceiptClient($client[0]->id_client);
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				$time = strtotime($key->updated_at);
				if($key->updated_at != null){
					$key->updated_at = date('d-m-Y',$time);
				} else {
					$key->updated_at = '';
				}
			}
			echo json_encode($data);
		}		
	}
	function uploadLogo($url){
				$date = new DateTime();
				$config['file_name']          = $date->getTimestamp().random_string('alnum', 5);
                $config['upload_path']          = 'assets/img/bukti/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 2000;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('bukti'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
                return $url;
	}	
}
