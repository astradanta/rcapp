<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_log','',TRUE);
	}

	public function index()
	{
		$this->load->view('Client/static/header');
		$this->load->view('Client/static/navbar');
		$this->load->view('Client/home');
		$this->load->view('Client/static/footer');		
	}
	function logout(){
		$date = date('Y-m-d');
		$this->m_log->insertLog($_SESSION['idClient'],'Logout',$date);		
		unset($_SESSION['namaClient']);
		unset($_SESSION['statusClient']);
		unset($_SESSION['emailClient']);
		unset($_SESSION['idClient']);
		$result = array();
		$result['status'] = 1;
		$result['message'] = "Anda berhasil keluar";
		echo json_encode($result);
	}

	function listPekerjaan(){
		$id = $_SESSION['idClient'];
		$client = $this->m_client->getClientByUser($id);
		$data = $this->m_pekerjaan_client->getListPekerjaan($client[0]->id_client);	
		echo json_encode($data);
	}
}