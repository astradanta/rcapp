<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_ManageKerja extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		if(isset($_POST['idKerja'])){
			$id_client = $_POST['idClient'];
			$id_kerja = $_POST['idKerja'];
			$id_pekerjaan = $_POST['idPekerjaan'];
			$data["id_client"] = $id_client;
			$data["id_pekerjaan"] = $id_pekerjaan;
			$tempClient = $this->m_client->getDetailClient($id_client);
			$temp = $this->m_master->getDetailDataPekerjaan($data["id_pekerjaan"]);
			$data["namaPekerjaan"] = $temp[0]->nama_pekerjaan;	
			$data['namaClient']		= $tempClient[0]->nama_client;
			$this->load->view('Back_office/static/header2',$data);
			// $this->load->view('Back_office/static/navbar');
			$this->load->view('Back_office/static/sidebar');
			$this->load->view('Back_office/manageKerja');
			$this->load->view('Back_office/static/footer2');						
		}else {
			redirect(base_url());
		}

	}

	function add(){
		$result["status"] = "failed";
		if (isset($_POST['access'])) {
			$created_at = date('Y-m-d h:i:s');
			$id_kerja = $_POST['id_kerja'];
			$id_detail_pekerjaan = $_POST['id_pekerjaan'];
			$url = $_POST['url'];
			$insert = $this->m_pekerjaan_client->insert_tb_detail_kerja($id_kerja,$id_detail_pekerjaan,$url,$created_at);
			$data = $this->m_master->spesificDetailPekerjaan($id_detail_pekerjaan);
			if($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Upload '.$data[0]->nama_detail_pekerjaan,$date);
			}
		}
		echo json_encode($result);		
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_detail_kerja = $_POST['idDetail'];
			$id_kerja = $_POST['id_kerja'];
			$id_detail_pekerjaan = $_POST['id_pekerjaan'];
			$url = $_POST['url'];			
			$updated_at = date('Y-m-d h:i:s');
			$updated_by = $_SESSION["nama"];
			$edit = $this->m_pekerjaan_client->edit_tb_detail_kerja($id_detail_kerja,$id_kerja,$id_detail_pekerjaan,$url,$updated_at,$updated_by);
			$data = $this->m_master->spesificDetailPekerjaan($id_detail_pekerjaan);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ulangi upload '.$data[0]->nama_detail_pekerjaan,$date);				
			}
		}
		echo json_encode($result);		
	}
	function changeStatus(){
		$result['status'] = "failed";
		if(isset($_POST['access'])){
			$id = $_POST['id'];
			$change = $this->m_pekerjaan_client->changeStatus($id);
			if($change){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Mengganti status pekerjaan menjadi selesai',$date);
			}
		}
		echo json_encode($result);
	}	
}	
