<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Pekerjaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_user','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$user = $this->m_user->allUser();
		$taxUser = array();
		$finaceUser = array();
		foreach ($user as $key) {
			if(($key->id_role == 3 )||($key->id_role==5)){
				array_push($taxUser, $key);
			}
			if(($key->id_role == 4 )||($key->id_role==6)){
				array_push($finaceUser, $key);
			}
		}
		$data['tax_user'] = $taxUser;
		$data['finance_user'] = $finaceUser;
		$this->load->view('Back_office/static/header2',$data);
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/pekerjaan');
		$this->load->view('Back_office/static/footer2');
	}

	function listPekerjaan(){
		if(isset($_POST['baselink'])){
			$data = $this->m_master->getListPekerjaan();
			$result = array();
				foreach ($data as $key ) {
					if($key->updated_by == null){
						$key->updated_by = "";
					}
					$time = strtotime($key->updated_at);
					$updated_at = date("d-m-Y",$time);
					if(is_null($key->updated_at))$updated_at = "";
					$time = strtotime($key->created_at);

					array_push($result, array("id_pekerjaan"=>$key->id_pekerjaan,"nama_pekerjaan"=>$key->nama_pekerjaan,"deskripsi_pekerjaan"=>$key->deskripsi_pekerjaan,"updated_at"=>$updated_at,"created_at"=>date('d-m-Y',$time),"updated_by"=>$key->updated_by,"kategori"=>$key->kategori,"tipe"=>$key->tipe));
				}
				echo json_encode($result);
		}
	}
	function addPekerjaan(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$nama = $_POST['nama'];
			$deskripsi = $_POST['deskripsi'];
			$created_at = date('Y-m-d h:i:s');
			$kategori = $_POST['kategori'];
			$tipe = $_POST['tipe'];
			$insert = $this->m_master->insertPekerjaan($nama,$deskripsi,$created_at,$kategori,$tipe);
			if ($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah pekerjaan '.$nama,$date);
			}
		}
		echo json_encode($result);		
	}
	function detailPekerjaan(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_master->getDetailDataPekerjaan($id);
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				$time = strtotime($key->updated_at);
				if ($key->updated_at == null){
					$key->updated_at = "";
				} else {
					$key->updated_at = date('d-m-Y',$time);
				}
			}
			echo json_encode($data);
		}
		
	}
	function editPekerjaan(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_pekerjaan = $_POST['idPekerjaan'];
			$nama = $_POST['nama'];
			$deskripsi = $_POST['deskripsi'];
			$updated_at = date('Y-m-d h:i:s');
			$updated_by = $_SESSION["nama"];
			$kategori = $_POST['kategori'];
			$tipe = $_POST['tipe'];
			$edit = $this->m_master->editPekerjaan($id_pekerjaan,$nama,$deskripsi,$updated_at,$updated_by,$kategori,$tipe);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Pekerjaan '.$nama,$date);				
			}
		}
		echo json_encode($result);		
		
	}
	function deletePekerjaan(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_master->getDetailDataPekerjaan($id);
			$delete = $this->m_master->deletePekerjaan($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Pekerjaan '.$data[0]->nama_pekerjaan,$date);				
			}
		}
		echo json_encode($result);
	}

}
