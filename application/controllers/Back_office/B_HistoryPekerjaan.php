<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_HistoryPekerjaan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);				
		$this->load->helper('string');
	}
	public function index()
	{	
		$this->load->view('Back_office/static/header2');
		//$this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/historyPekerjaan');
		$this->load->view('Back_office/static/footer2');
		
	}
	function list(){
		if (isset($_POST['baselink'])){
			$data = $this->m_pekerjaan_client->getHistoryPekerjaan();
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('m/d/Y',$time);
			}
			echo json_encode($data);
		}	
	}
	function search(){
		if (isset($_POST['baselink'])){
			$keyword = $_POST['keyword'];
			$data = $this->m_pekerjaan_client->searchtHistoryPekerjaan($keyword);
			foreach ($data as $key) {
				$time = strtotime($key->upload_date);
				$key->upload_date = date('d-m-Y',$time);
				$time = strtotime($key->reupload_date);
				if($key->reupload_date == null){
					$key->reupload_date = "";
				}else {
					$key->reupload_date = date('d-m-Y',$time);
				}
			}
			echo json_encode($data);
		}	
	}
	function detail(){
		$this->load->view('Back_office/static/header2');
		//$this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/manageKerja');
		$this->load->view('Back_office/static/footer2');		
	}	

}
