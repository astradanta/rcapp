<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_DetailHistory extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);				
		$this->load->helper('string');
	}
	public function index()
	{	
			$id = $this->uri->segment(2);
			$historyKerja = $this->m_pekerjaan_client->getDetailHistoryPekerjaan($id);
			if (sizeof($historyKerja)==0){
				redirect(base_url().'detailHistory');
			}

			$id_client = $historyKerja[0]->id_client;
			$id_kerja = $historyKerja[0]->id_kerja;
			$id_pekerjaan = $historyKerja[0]->id_pekerjaan;
			$data["id_client"] = $id_client;
			$data["id_pekerjaan"] = $id_pekerjaan;
			$tempClient = $this->m_client->getDetailClient($id_client);
			$temp = $this->m_master->getDetailDataPekerjaan($data["id_pekerjaan"]);
			$data["namaPekerjaan"] = $temp[0]->nama_pekerjaan;	
			$data['namaClient']		= $tempClient[0]->nama_client;
			$this->load->view('Back_office/static/header2',$data);
			// $this->load->view('Back_office/static/navbar');
			$this->load->view('Back_office/static/sidebar');
			$this->load->view('Back_office/detailHistory');
			$this->load->view('Back_office/static/footer2');		
	}

}
