<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_DetailPekerjaan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_master','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		//$this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/detailPekerjaan');
		$this->load->view('Back_office/static/footer2');
	}

	function list(){
		if(isset($_POST['baselink'])){
			$data = $this->m_master->getAllDetailPekerjaan();
			$result = array();
			foreach ($data as $key) {
					if($key->updated_by == null){
						$key->updated_by = "";
					}
					$jenis_pekerjaan = "Klien Upload";
					if ($key->jenis_pekerjaan == 1){
						$jenis_pekerjaan = "Klien Download";
					}
					$time = strtotime($key->updated_at);
					$updated_at = date("d-m-Y",$time);
					if(is_null($key->updated_at))$updated_at = "";
					$time = strtotime($key->created_at);
					array_push($result, array("id_detail_pekerjaan"=>$key->id_detail_pekerjaan,"nama_detail_pekerjaan"=>$key->nama_detail_pekerjaan,"deskripsi"=>$key->deskripsi,"updated_at"=>$updated_at,"created_at"=>date('d-m-Y',$time),"updated_by"=>$key->updated_by,"nama_pekerjaan"=>$key->nama_pekerjaan,"jenis_pekerjaan"=>$jenis_pekerjaan));				
			}
			echo json_encode($result);
		}
	}
	function add(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$nama = $_POST['nama'];
			$deskripsi = $_POST['deskripsi'];
			$idPekerjaan = $_POST['pekerjaan'];
			$jenis_pekerjaan = $_POST['jenisPekerjaan'];
			$created_at = date('Y-m-d h:i:s');
			$insert = $this->m_master->insertDetail($nama,$deskripsi,$created_at,$idPekerjaan,$jenis_pekerjaan);
			if ($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah pekerjaan '.$nama,$date);
			}			
		}
		echo json_encode($result);		
	}
	function detail(){
		if(isset($_POST['baselink'])){
			$id = $_POST["id"];
			$data = $this->m_master->spesificDetailPekerjaan($id);
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				$time = strtotime($key->updated_at);
				if ($key->updated_at == null){
					$key->updated_at = "";
				} else {
					$key->updated_at = date('d-m-Y',$time);
				}
			}			
			echo json_encode($data);
		}
		
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_detail_pekerjaan = $_POST['idPekerjaan'];
			$nama = $_POST['nama'];
			$deskripsi = $_POST['deskripsi'];
			$idPekerjaan = $_POST['pekerjaan'];
			$jenis_pekerjaan = $_POST['jenisPekerjaan'];
			$updated_at = date('Y-m-d h:i:s');
			$updated_by = $_SESSION["nama"];
			$edit = $this->m_master->editSpesificDetail($id_detail_pekerjaan,$nama,$deskripsi,$idPekerjaan,$jenis_pekerjaan,$updated_at,$updated_by);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Pekerjaan '.$nama,$date);				
			}
		}
		echo json_encode($result);		
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_master->spesificDetailPekerjaan($id);
			$delete = $this->m_master->deleteSpesificPekerjaan($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Pekerjaan '.$data[0]->nama_detail_pekerjaan,$date);				
			}
		}
		echo json_encode($result);
	}

}
