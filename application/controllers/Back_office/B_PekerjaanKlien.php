<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_PekerjaanKlien extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_client','',TRUE);
		$this->load->model('m_pekerjaan_client','',TRUE);				
		$this->load->helper('string');
	}
	public function index()
	{	
	    $idPekerjaan = $this->uri->segment(2);
		$temData = $this->m_client->getDetailClient($idPekerjaan);
		$data["idPekerjaanKlien"] = $idPekerjaan;
		$data["namaKlien"] = $temData[0]->nama_client;
		$data['client'] = $temData[0];
		$this->load->view('Back_office/static/header2',$data);
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/pekerjaanKlien');
		$this->load->view('Back_office/static/footer2');
		
	}

	function list(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_pekerjaan_client->getListPekerjaan($id);
			echo json_encode($data);
		}
	}

	function listSelectPekerjaan(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_pekerjaan_client->listPekerjaanKlien($id);
			echo json_encode($data);
		}		
	}
	function listPekerjaanKlien(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_client->getPekerjaanClient($id);
			echo json_encode($data);
		}	
	}
	function add(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$idPekerjaan = $_POST['pekerjaan'];
			$created_at = date('Y-m-d h:i:s');
			$time = strtotime($_POST['startDate']);
			$startDate = date('Y-m-d',$time);
			$time = strtotime($_POST['deadLine']);
			$deadLine = date('Y-m-d',$time);
			$insert = $this->m_pekerjaan_client->insert($idPekerjaan,$startDate,$deadLine,$created_at);
			if ($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah Pekerjaan Klien ',$date);				
			}
		}
		echo json_encode($result);		
	}
	function detail(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_pekerjaan_client->detail($id);
			$time = strtotime($data[0]->start_date);
			$data[0]->start_date = date('m/d/Y',$time);
			$time = strtotime($data[0]->deadline);
			$data[0]->deadline = date('m/d/Y',$time);
			echo json_encode($data);			
		}
		
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$idDetail = $_POST['idDetail'];
			$pekerjaan = $_POST['pekerjaan'];
			$time = strtotime($_POST['startDate']);
			$startDate = date('Y-m-d',$time);
			$time = strtotime($_POST['deadLine']);
			$deadLine = date('Y-m-d',$time);
			$updated_at = date('Y-m-d h:i:s');
			$edit = $this->m_pekerjaan_client->edit($idDetail,$pekerjaan,$startDate,$deadLine,$updated_at);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Pekerjaan ',$date);				
			}			
		}
		echo json_encode($result);		
		
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$delete = $this->m_pekerjaan_client->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Pekerjaan ',$date);				
			}
		}
		echo json_encode($result);
	}

}
