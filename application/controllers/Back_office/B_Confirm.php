<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Confirm extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_confirm','',TRUE);
		$this->load->model('m_invoice','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/confirm');
		$this->load->view('Back_office/static/footer2');
	}

	function list(){
		if(isset($_POST['baselink'])){
			$data = $this->m_confirm->listAdminPanel();	
			foreach ($data as $key) {
					$key->file_bukti = base_url().$key->file_bukti;
				}	
			echo json_encode($data);
		}
	}
	function changeStatus(){
		$result['status'] = "failed";
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$oldData = $this->m_confirm->detail($id);
			$status = $_POST['status'];
			$id_user = $_SESSION['id'];
			$change = $this->m_confirm->changeStatus($id,$status,$id_user);
			if($change){
				if($status == "Gagal"){
					$this->m_invoice->changeStatus($oldData[0]->id_invoice,'Belum dibayar');
				} else {
					$this->m_invoice->changeStatus($oldData[0]->id_invoice,'Terbayar');
				}
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Konfirmasi Pembayaran Invoice '.$oldData[0]->no_invoice.' menjadi '.$status,$date);	
				$result['status'] = "success";			

			}
		}
		echo json_encode($result);

	}

}
