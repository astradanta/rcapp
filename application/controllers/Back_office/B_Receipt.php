<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Receipt extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_receipt','',TRUE);
		$this->load->model('m_invoice','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/receipt');
		$this->load->view('Back_office/static/footer2');
	}
	function list(){
		if (isset($_POST['baselink'])){
			$data = $this->m_receipt->list();
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				$time = strtotime($key->updated_at);
				if($key->updated_at != null){
					$key->updated_at = date('d-m-Y',$time);
				} else {
					$key->updated_at = '';
				}
			}
			echo json_encode($data);
		}
	}
	function listInvoice(){
		if(isset($_POST['baselink'])){
			$data = $this->m_invoice->receiptNewList();
			echo json_encode($data);			
		}
	}
	function add(){
		$result['status'] = "failed";
		if(isset($_POST['access'])){
			$id_user = $_SESSION['id'];
			$id_invoice = $_POST['pekerjaan'];
			$deskripsi = $_POST['deskripsi'];
			$oldData = $this->m_invoice->detail($id_invoice);
			$url = $_POST['url'];
			$created_at = date('Y-m-d h:i:s');
			$insert = $this->m_receipt->insert($id_user,$id_invoice,$url,$deskripsi,$created_at);
			if($insert){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Menambahkan receipt untuk Invoice '.$oldData[0]->no_invoice,$date);

			}

		}
		echo json_encode($result);
	}
	function detail(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_receipt->detail($id);
			echo json_encode($data);
		}
	}

	function edit(){
		$result['status'] = "failed";
		if(isset($_POST['access'])){
			$id_receipt = $_POST['idDetail'];
			$id_user = $_SESSION['id'];
			$id_invoice = $_POST['pekerjaan'];
			$deskripsi = $_POST['deskripsi'];
			$oldData = $this->m_invoice->detail($id_invoice);
			$url = $_POST['url'];
			$updated_at = date('Y-m-d h:i:s');
			$edit = $this->m_receipt->edit($id_receipt,$id_user,$id_invoice,$url,$deskripsi,$updated_at);
			if($edit){
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Mengubah receipt untuk Invoice '.$oldData[0]->no_invoice,$date);

			}
		}
		echo(json_encode($result));
	}

	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_invoice->detail($id);
			$delete = $this->m_receipt->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Receipt untuk Invoice '.$data[0]->no_invoice,$date);				
			}
		}
		echo json_encode($result);		
	}

}
