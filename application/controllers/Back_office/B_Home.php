<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Home extends CI_Controller {

	public function index()
	{
		$this->load->view('Back_office/static/header');
		$this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/home');
		$this->load->view('Back_office/static/footer');
	}
}