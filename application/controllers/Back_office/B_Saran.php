<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Saran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_saran','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/saran');
		$this->load->view('Back_office/static/footer2');
	}
	function list(){
		if(isset($_POST['baselink'])){
			$data = $this->m_saran->list();
			foreach ($data as $key) {
				$time = strtotime($key->create_at);
				$key->create_at = date('d-m-Y',$time);
				$time = strtotime($key->update_at);
				if ($key->update_at == null){
					$key->update_at = "";
				} else {
					$key->update_at = date('d-m-Y',$time);
				}
			}
			echo(json_encode($data));
		}

	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$delete = $this->m_saran->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus saran',$date);				
			}
		}
		echo json_encode($result);
	}
}
