<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->library('email');
	}
	public function index()
	{
		$this->load->view('Back_office/static/headerLogin');
		$this->load->view('Back_office/login');
		$this->load->view('Back_office/static/footerLogin');
		if(isset($_SESSION['nama'])){
			redirect(base_url());
		}
	}
	function signIn(){
		$result = array();
		$result['status'] = 0;
		$result['message'] = 'Kekurangan Beberapa Parameter Fungsi';
		if (isset($_POST['email']) && isset($_POST['password'])) {
			$email = $_POST['email'];
			$password = $_POST['password'];
			$data = $this->m_user->getUserByEmail($email);
			if (sizeof($data) == 0) {
				$result['message'] = "Anda Belum Terdaftar";
			} else {
				$dataStatus = $data[0]->jenis_user;
				$dataPassword = $this->encryption->decrypt($data[0]->password);
				$dataRoleName = $data[0]->role_name;
				$dataRoleId = $data[0]->id_role;
				$dataPhoto = base_url()."assets/img/account.png";
				if(($data[0]->photo != "")&&($data[0]->photo != null)){
					$dataPhoto = base_url().$data[0]->photo;
				}
				if ($dataStatus < 1) {
					$result['message'] = "Anda tidak memiliki akses pada halaman ini";
				}else if($password != $dataPassword){
					$result['message'] = 'Password anda salah';
				} else {
					$result['status'] = 1;
					$result['message'] = 'Login Berhasil';
					$_SESSION['nama'] = $data[0]->nama;
					$_SESSION['email'] = $email;
					$_SESSION['status'] = $dataStatus;
					$_SESSION['role_name'] = $dataRoleName;
					$_SESSION['role_id'] = $dataRoleId;
					$_SESSION['photo'] = $dataPhoto;
					$_SESSION['password'] = $password;
					$_SESSION['id'] = $data[0]->id_user;
					$date = date('Y-m-d');
					$this->m_log->insertLog($data[0]->id_user,'Login',$date);
				}
			}
		}
		echo json_encode($result);
	}

	function forget(){
		$result = array();
		$result['status'] = 0;
		$result['message'] = 'Kekurangan Beberapa Parameter Fungsi';
		if (isset($_POST['email'])){
			$email = $_POST['email'];
			 $data = $this->m_user->getUserByEmail($email);
			 if (sizeof($data) == 0) {
				$result['message'] = "Anda Belum Terdaftar";
			} else {
				$dataStatus = $data[0]->jenis_user;
				if ($dataStatus < 1) {
					$result['message'] = "Anda tidak memiliki akses pada halaman ini";
				} else {
					$result['status'] = 1;
					$result['message'] = 'Pengiriman berhasil, silakan cek email anda';
					$name = $data[0]->nama;
					$password = $this->encryption->decrypt($data[0]->password);
                     $this->sendPasswordWithEmail($name,$email,"redconsulting@gmail.com","RED Consulting",$password);
				}
			}
		}
		echo json_encode($result);	

	}
	function sendPasswordWithEmail($name,$to,$from,$fromName,$password){
        $htmlmessage="<!DOCTYPE html> 
						<head> 
					    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
						</head>
						<body> 

						<table cellpadding='10' style='border-color:#666' rules='all'>
							<tbody>
								<tr style='background-color:#393e40;color:white;font-size:14px'>
								<td colspan='2'></td>
								</tr>
								<tr>
									<td><p><MyFont12>Email</MyFont12></p></td>
									<td style='width:400px;'><p><MyFont12>$to</MyFont12></p></td>
								</tr>
								
								<tr>
									<td><p><MyFont12>Password</MyFont12></p></td>
									<td><p><MyFont12>$password</MyFont12></p></td>
								</tr>
							</tbody>
						</table>
					 <br> <br>
									
							  </body>
							  </html>

					";
		$this->email->from($from, 'RED Consulting');
		$this->email->to($to);
		$this->email->subject('Your Account Access');
		$this->email->set_mailtype("html");
		$this->email->message($htmlmessage);

		$this->email->send();
	}
}
