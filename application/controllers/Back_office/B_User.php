<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_user','',TRUE);
		$this->load->model('m_log','',TRUE);
		$this->load->helper('string');
	}
	public function index()
	{
		$role = $this->m_user->allRole();
		$data['role'] = $role;
		$this->load->view('Back_office/static/header2',$data);
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/user');
		$this->load->view('Back_office/static/footer2');
	}

	function listUser(){
		if(isset($_POST['baselink'])){
			$data = $this->m_user->allUser();
				$result = array();
				foreach ($data as $key ) {
					$jenis_user = "Klien";
					if($key->jenis_user==1){$jenis_user=$key->role_name;};
					$time = strtotime($key->updated_at);
					$updated_at = date("d-m-Y",$time);
					if(is_null($key->updated_at))$updated_at = "";
					$time = strtotime($key->created_at);
					array_push($result, array("id_user"=>$key->id_user,"jenis_user"=>$jenis_user,"nama"=>$key->nama,"email"=>$key->email,"updated_at"=>$updated_at,"created_at"=>date('d-m-Y',$time)));
				}
				echo json_encode($result);
		}
	}
	function addUser(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$nama = $_POST['nama'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$jenis = $_POST['addJenis'];
			$id_role = $jenis;
			$jenis = 1;
			$password = $this->encryption->encrypt($password);
			$created_at = date('Y-m-d h:i:s');
			$logo = '';
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
			}
			$insert = $this->m_user->insertUser($nama,$email,$password,$jenis,$created_at,$logo,$id_role);
			if ($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah user '.$nama,$date);				
			}
		}
		echo json_encode($result);
	}
	function detailUser(){
		if(isset($_POST['baselink'])){
			$id = $_POST['id'];
			$data = $this->m_user->getDetail($id);
			$data[0]->password = $this->encryption->decrypt($data[0]->password);
			foreach ($data as $key) {
				$time = strtotime($key->created_at);
				$key->created_at = date('d-m-Y',$time);
				$time = strtotime($key->updated_at);
				if ($key->updated_at == null){
					$key->updated_at = "";
				} else {
					$key->updated_at = date('d-m-Y',$time);
				}
			}
			echo json_encode($data);
		}
		
	}
	function editUser(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_user = $_POST['idUser'];
			$nama = $_POST['nama'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$jenis = "0";
			if(isset($_POST['editJenis'])){
				$jenis = $_POST['editJenis'];
			}
			
			$id_role = null;
			if($jenis != 0){
				$id_role = $jenis;
				$jenis = 1;
			}
			$password = $this->encryption->encrypt($password);
			$updated_at = date('Y-m-d h:i:s');
			$data = $this->m_user->getDetail($id_user);
			$oldLogo = $data[0]->photo;
			$logo = $oldLogo;			
			if ($_FILES['logo']['name'] != ""){
				$logo = $this->uploadLogo($logo);
				if ($logo != $oldLogo){
					if(file_exists(base_url().$oldLogo)){
						unlink(base_url().$oldLogo);
					}
				}
			}			
			$edit = $this->m_user->editUserWithPassword($id_user,$nama,$email,$password,$jenis,$updated_at,$logo,$id_role);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah User '.$nama,$date);				
			}
			if(isset($_POST['global'])){
					$_SESSION['nama'] = $nama;
					$_SESSION['email'] = $email;
					$_SESSION['photo'] = $logo;
					$_SESSION['password'] = $_POST['password'];
					$_SESSION['id'] = $id_user;				
			}
		}
		echo json_encode($result);		
	}
	function deleteUser(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_user->getDetail($id);
			$delete = $this->m_user->deleteUser($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus User '.$data[0]->nama,$date);				
			}
		}
		echo json_encode($result);
	}
	function generatePassword(){
		if(isset($_POST['baselink'])){
			echo random_string('alnum', 8);
		}
	}
	function uploadLogo($url){
				$date = new DateTime();
				$config['file_name']          = $date->getTimestamp().random_string('alnum', 5);
                $config['upload_path']          = 'assets/img/client/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 2000;
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $url = $config['upload_path'].$data['upload_data']['orig_name'];
                }		
                return $url;
	}
}
