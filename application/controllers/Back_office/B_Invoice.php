<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Invoice extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('m_log','',TRUE);
		$this->load->model('m_invoice','',TRUE);
	}
	public function index()
	{
		$this->load->view('Back_office/static/header2');
		// $this->load->view('Back_office/static/navbar');
		$this->load->view('Back_office/static/sidebar');
		$this->load->view('Back_office/invoice');
		$this->load->view('Back_office/static/footer2');
		if(!isset($_SESSION['nama'])){
			redirect(base_url().'login');
		}
	}

	function list()	{
		if (isset($_POST['baselink'])){
			$data = $this->m_invoice->list();
			echo json_encode($data);
		}
	}

	function add(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$no_invoice = $_POST['no_invoice'];
			$id_client = $_POST['pekerjaan'];
			$time = strtotime($_POST['tanggal']);
			$tanggal = date('Y-m-d',$time);
			$time = strtotime($_POST['due']);
			$due = date('Y-m-d',$time);
			$total = $_POST['total'];
			$url = $_POST['url'];
			$note = $_POST['note'];
			$created_at = date('Y-m-d h:i:s');
			$insert = $this->m_invoice->insert($no_invoice,$id_client,$tanggal,$due,$total,$url,$note,$created_at);
			if ($insert){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Tambah invoice '.$no_invoice,$date);
			}			
		}
		echo json_encode($result);		
	}
	function getNoInvoice(){
		$result['no_invoice'] = $this->m_invoice->getid();
		echo json_encode($result);
	}
	function detail(){
		if(isset($_POST['baselink'])){
			$id = $_POST["id"];
			$data = $this->m_invoice->detail($id);
			echo json_encode($data);
		}
		
	}
	function edit(){
		$result["status"] = "failed";
		if (isset($_POST['access'])){
			$id_invoice = $_POST['idDetail'];
			$no_invoice = $_POST['no_invoice'];
			$id_client = $_POST['pekerjaan'];
			$time = strtotime($_POST['tanggal']);
			$tanggal = date('Y-m-d',$time);
			$time = strtotime($_POST['due']);
			$due = date('Y-m-d',$time);
			$total = $_POST['total'];
			$url = $_POST['url'];
			$note = $_POST['note'];
			$updated_at = date('Y-m-d h:i:s');
			$edit = $this->m_invoice->edit($id_invoice,$no_invoice,$id_client,$tanggal,$due,$total,$url,$note,$updated_at);
			if ($edit){
				$result["status"] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Ubah Invoice '.$no_invoice,$date);				
			}
		}
		echo json_encode($result);		
	}
	function delete(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_invoice->detail($id);
			$delete = $this->m_invoice->delete($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Invoice '.$data[0]->no_invoice,$date);				
			}
		}
		echo json_encode($result);
	}
	function cancel(){
		$result["status"] = "failed";
		if (isset($_POST['baselink'])) {
			$id = $_POST['id'];
			$data = $this->m_invoice->detail($id);
			$delete = $this->m_invoice->cancel($id);
			if ($delete) {
				$result['status'] = "success";
				$date = date('Y-m-d');
				$this->m_log->insertLog($_SESSION['id'],'Hapus Invoice '.$data[0]->no_invoice,$date);				
			}
		}
		echo json_encode($result);
	}
	function archive(){
		if (isset($_POST['baselink'])){
			$data = $this->m_invoice->archive();
			echo json_encode($data);
		}		
	}				
}