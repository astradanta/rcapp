<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'B_Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'Back_office/B_Login';
$route['login/signin'] = 'Back_office/B_Login/signIn';
$route['login/forget'] = 'Back_office/B_Login/forget';
$route['logout'] = 'B_Home/logout';
$route['listClient'] = 'B_Home/listClient';
$route['listPekerjaan'] = 'B_Home/listPekerjaan';
$route['filterClient'] = 'B_Home/filterClient';
$route['addClient'] = 'B_Home/addClient';
$route['detailClient'] = 'B_Home/detailClient';
$route['editClient'] = 'B_Home/editClient';
$route['deleteClient'] = 'B_Home/deleteClient';
$route['countElement'] = 'B_Home/countElement';
$route['home'] = 'B_Home';

// Back office log //
$route['log'] = 'Back_office/B_Log';
$route['log/loadData'] = 'Back_office/B_Log/loadData';
$route['log/currentLog'] = 'Back_office/B_Log/currentLog';

// Back  office user//
$route['user'] = 'Back_office/B_User';
$route['user/listUser'] = 'Back_office/B_User/listUser';
$route['user/addUser'] = 'Back_office/B_User/addUser';
$route['user/detailUser'] = 'Back_office/B_User/detailUser';
$route['user/editUser'] = 'Back_office/B_User/editUser';
$route['user/deleteUser'] = 'Back_office/B_User/deleteUser';
$route['user/generatePassword'] = 'Back_office/B_User/generatePassword';

// Back  office pekerjaan//
$route['pekerjaan'] = 'Back_office/B_Pekerjaan';
$route['pekerjaan/listPekerjaan'] = 'Back_office/B_Pekerjaan/listPekerjaan';
$route['pekerjaan/addPekerjaan'] = 'Back_office/B_Pekerjaan/addPekerjaan';
$route['pekerjaan/detailPekerjaan'] = 'Back_office/B_Pekerjaan/detailPekerjaan';
$route['pekerjaan/editPekerjaan'] = 'Back_office/B_Pekerjaan/editPekerjaan';
$route['pekerjaan/deletePekerjaan'] = 'Back_office/B_Pekerjaan/deletePekerjaan';

// Back  office detail pekerjaan//
$route['detailPekerjaan'] = 'Back_office/B_DetailPekerjaan';
$route['detailPekerjaan/list'] = 'Back_office/B_DetailPekerjaan/list';
$route['detailPekerjaan/add'] = 'Back_office/B_DetailPekerjaan/add';
$route['detailPekerjaan/detail'] = 'Back_office/B_DetailPekerjaan/detail';
$route['detailPekerjaan/edit'] = 'Back_office/B_DetailPekerjaan/edit';
$route['detailPekerjaan/delete'] = 'Back_office/B_DetailPekerjaan/delete';

// Back  office pekerjaan klien//
$route['pekerjaanKlien/:num'] = 'Back_office/B_PekerjaanKlien';
$route['pekerjaanKlien/list'] = 'Back_office/B_PekerjaanKlien/list';
$route['pekerjaanKlien/add'] = 'Back_office/B_PekerjaanKlien/add';
$route['pekerjaanKlien/detail'] = 'Back_office/B_PekerjaanKlien/detail';
$route['pekerjaanKlien/edit'] = 'Back_office/B_PekerjaanKlien/edit';
$route['pekerjaanKlien/delete'] = 'Back_office/B_PekerjaanKlien/delete';
$route['pekerjaanKlien/listSelectPekerjaan'] = 'Back_office/B_PekerjaanKlien/listSelectPekerjaan';
$route['pekerjaanKlien/listPekerjaanKlien'] = 'Back_office/B_PekerjaanKlien/listPekerjaanKlien';

//Back office history pekerjaan//
$route['historyPekerjaan'] = 'Back_office/B_HistoryPekerjaan';
$route['historyPekerjaan/list'] = 'Back_office/B_HistoryPekerjaan/list';
$route['historyPekerjaan/search'] = 'Back_office/B_HistoryPekerjaan/search';
$route['detailHistory/:num'] = 'Back_office/B_DetailHistory';



//Back office history pekerjaan//
$route['invoice'] = 'Back_office/B_Invoice';
$route['invoice/list'] = 'Back_office/B_Invoice/list';
$route['invoice/archive'] = 'Back_office/B_Invoice/archive';
$route['invoice/getNoInvoice'] = 'Back_office/B_Invoice/getNoInvoice';
$route['invoice/add'] = 'Back_office/B_Invoice/add';
$route['invoice/detail'] = 'Back_office/B_Invoice/detail';
$route['invoice/edit'] = 'Back_office/B_Invoice/edit';
$route['invoice/delete'] = 'Back_office/B_Invoice/delete';
$route['invoice/cancel'] = 'Back_office/B_Invoice/cancel';

//Back office konfirmasi pembayaran//
$route['confirm'] = 'Back_office/B_Confirm';
$route['confirm/list'] = 'Back_office/B_Confirm/list';
$route['confirm/changeStatus'] = 'Back_office/B_Confirm/changeStatus';

//Back office konfirmasi pembayaran//
$route['receipt'] = 'Back_office/B_Receipt';
$route['receipt/list'] = 'Back_office/B_Receipt/list';
$route['receipt/listInvoice'] = 'Back_office/B_Receipt/listInvoice';
$route['receipt/add'] = 'Back_office/B_Receipt/add';
$route['receipt/detail'] = 'Back_office/B_Receipt/detail';
$route['receipt/edit'] = 'Back_office/B_Receipt/edit';
$route['receipt/delete'] = 'Back_office/B_Receipt/delete';

$route['saran'] = 'Back_office/B_Saran';
$route['saran/list'] = 'Back_office/B_Saran/list';
$route['saran/delete'] = 'Back_office/B_Saran/delete';


$route['manageKerja'] = 'Back_office/B_ManageKerja';
$route['manageKerja/add'] = 'Back_office/B_ManageKerja/add';
$route['manageKerja/edit'] = 'Back_office/B_ManageKerja/edit';
$route['manageKerja/changeStatus'] = 'Back_office/B_ManageKerja/changeStatus';

$route['manageClient'] = 'Back_office/B_Client';
$route['manageClient/countElement'] = 'Back_office/B_Client/countElement';
$route['manageClient/pekerjaanKlien'] = 'Back_office/B_Client/pekerjaanKlien';
$route['manageClient/savePekerjaan'] = 'Back_office/B_Client/savePekerjaan';
//CLIENT//


// Client Route
$route['client'] = 'Client/C_Home';
$route['client/login'] = 'Client/C_Login';
$route['client/signin'] = 'Client/C_Login/signIn';
$route['client/forget'] = 'Client/C_Login/forget';
$route['client/logout'] = 'Client/C_Home/logout';
$route['client/listPekerjaan'] = 'Client/C_Home/listPekerjaan';

$route['sendEmail'] = 'Back_office/B_Login/sendPasswordWithEmail';

// Client log //
$route['client/log'] = 'Client/C_Log';
$route['client/log/loadData'] = 'Client/C_Log/loadData';


// client pekerjaan //
// Back  office detail pekerjaan//
$route['client/pekerjaan/:num/:num/:num'] = 'Client/C_Pekerjaan';
$route['client/pekerjaan/getOnProgress'] = 'Client/C_Pekerjaan/getOnProgress';
$route['client/pekerjaan/add'] = 'Client/C_Pekerjaan/add';
$route['client/pekerjaan/edit'] = 'Client/C_Pekerjaan/edit';
$route['client/pekerjaan/getArchive'] = 'Client/C_Pekerjaan/getArchive';

//client invoice//
$route['client/invoice'] = 'Client/C_Invoice';
$route['client/invoice/list'] = 'Client/C_Invoice/list';
$route['client/invoice/new'] = 'Client/C_Invoice/new';

//client konfirmasi//
$route['client/confirm'] = 'Client/C_Confirm';
$route['client/confirm/list'] = 'Client/C_Confirm/list';
$route['client/confirm/add'] = 'Client/C_Confirm/add';
$route['client/confirm/detail'] = 'Client/C_Confirm/detail';
$route['client/confirm/edit'] = 'Client/C_Confirm/edit';
$route['client/confirm/delete'] = 'Client/C_Confirm/delete';
$route['client/confirm/listReceipt'] = 'Client/C_Confirm/listReceipt';

$route['client/saran'] = 'Client/C_Saran';
$route['client/saran/list'] = 'Client/C_Saran/list';
$route['client/saran/add'] = 'Client/C_Saran/add';
$route['client/saran/edit'] = 'Client/C_Saran/edit';
$route['client/saran/delete'] = 'Client/C_Saran/delete';